<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeedUsuario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'usuario'=> 'Admin',
            'contraseña'=> 'contraseña',
            'tipo'=> 'alguntipo',
            'Fecha'=> '2018-04-10',
            'Activo'=> '1',
        ]);
    }
}
