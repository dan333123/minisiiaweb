<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'usuarios',
        ]);   
    }

    public function truncateTables(array $tables){
        DB::statement('set FOREIGN_KEY_CHECKS = 0');  
        //vamos a eliminar los datos de cada tabla que le pasemos en el arreglo
        foreach($tables as $table){
            DB::table($table)->truncate();
        }
        DB::statement('set FOREIGN_KEY_CHECKS = 1');
    }
}
