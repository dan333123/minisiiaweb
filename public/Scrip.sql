create DATABASE minisiia DEFAULT CHARACTER SET utf8 ;

use minisiia;

--Primero ejecuten la migracion para que se cree la tabla users, no olviden borrar la migraciones de roles y maestros

create table Roles(
id  int(10) unsigned NOT NULL,
Rol varchar(15) default 'Alumno',
Matricula integer ,
PRIMARY KEY (id),
CONSTRAINT fk_usuario_id_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
-- ------------------------------------------------------------------------
create table Semestre(
CodigoSemestre integer not null auto_increment,
Campus varchar(15) not null,
FechaInicio date not null,
FechaFin date not null,
CalifAprovatoria integer not null,
NoParciales integer default 70,
FechaCreacion datetime default NOW(),
Activo boolean default true,
id  int(10) unsigned NOT NULL,
PRIMARY KEY (CodigoSemestre, id),
INDEX fk_usuario_i (id ASC),
CONSTRAINT fk_usuario_semestre_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
-- ------------------------------------------------------------------------
create table Parcial(
CodigoSemestre integer not null,
NoParcial integer not null,
FechaInicio date not null,
FechaFin date not null,
CONSTRAINT fk_codigo_semestre_parcial_f
FOREIGN KEY (CodigoSemestre)
REFERENCES MiniSIIA.Semestre (CodigoSemestre)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- ------------------------------------------------------------------------
create table Maestro(
MatriculaMaestro integer not null,
nombre varchar(20) not null,
apellidoP varchar(20) not null,
apellidoM varchar(20),
Genero boolean DEFAULT true,
Departamento varchar(40) not null,
correo varchar(30) not null,
telefono long not null,
Activo boolean default true,
FechaCreacion datetime default NOW(),
id  int(10) unsigned NOT NULL,
PRIMARY KEY (MatriculaMaestro,id),
INDEX fk_usuario_maestro_i (id ASC),
CONSTRAINT fk_usuario_maestro_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
-- -------------------------------------------------------------------------
create table Materia(
clave integer not null auto_increment,
Nombre varchar(40) not null,
Carrera varchar(35) not null,
Unidades integer not null,
id  int(10) unsigned NOT NULL,
FechaCreacion datetime default NOW(),
PRIMARY KEY (clave,id),
INDEX fk_usuario_materia_i (id ASC),
CONSTRAINT fk_usuario_materia_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
/* 
SET foreign_key_checks = 0;
drop table curso;
SET foreign_key_checks = 1;*/
create table Curso(
ClaveMateria INTEGER not null auto_increment,
clave integer not null,
Grupo varchar(2) not null,
Salon varchar(8) not null,
MatriculaMaestro integer not null,
CodigoSemestre integer not null,
id  int(10) unsigned NOT NULL,
FechaCreacion datetime default NOW(),
PRIMARY KEY (ClaveMateria,clave,id,MatriculaMaestro,CodigoSemestre),
INDEX fk_usuario_curso_i (id ASC),
INDEX fk_clave_curso_i (clave ASC),
INDEX fk_matriculamaestro_curso_i (MatriculaMaestro ASC),
CONSTRAINT fk_usuario_curso_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_clave_curso_f
FOREIGN KEY (clave)
REFERENCES MiniSIIA.Materia (clave)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_matriculaMaestro_curso_f
FOREIGN KEY (MatriculaMaestro)
REFERENCES MiniSIIA.Maestro (MatriculaMaestro)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_CodigoSemestre_curso_f
FOREIGN KEY (CodigoSemestre)
REFERENCES MiniSIIA.Semestre (CodigoSemestre)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table Alumno(
MatriculaAlumno INTEGER not null,
nombre varchar(20) not null,
apellidoP varchar(20) not null,
apellidoM varchar(20),
Genero boolean DEFAULT true,
Carrera varchar(18) not null,
correo varchar(30) not null,
telefono long not null,
añoIngreso INTEGER,
mesIngreso INTEGER,
Activo boolean default true,
FechaCreacion datetime default NOW(),
id  int(10) unsigned NOT NULL,
PRIMARY KEY (MatriculaAlumno,id),
INDEX fk_usuario_Alumno_i (id ASC),
CONSTRAINT fk_usuario_alumno_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table HorarioMateria(
ClaveMateria INTEGER not null,
Dia VARCHAR(9) not null,
HoraInicio varchar(15) not null,
HoraFin varchar(15) not null,
PRIMARY KEY (ClaveMateria),
INDEX fk_ClaveMateria_HorarioMateria_i (ClaveMateria ASC),
CONSTRAINT fk_ClaveMateria_HorarioMateria_f
FOREIGN KEY (ClaveMateria)
REFERENCES MiniSIIA.curso (ClaveMateria)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table ReglaMateria(
ClaveMateria INTEGER not null,
NoParcial INTEGER not null,
UniPorParcial INTEGER not null,
Valor INTEGER not null,
PRIMARY KEY (ClaveMateria),
INDEX fk_ClaveMateria_ReglaMateria_i (ClaveMateria ASC),
CONSTRAINT fk_ClaveMateria_ReglaMateria_f
FOREIGN KEY (ClaveMateria)
REFERENCES MiniSIIA.curso (ClaveMateria)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table MateriaAlumno(
MatriculaAlumno INTEGER not null,
ClaveMateria INTEGER not null,
SemestreAlumno INTEGER not null,
Curso VARCHAR(15) DEFAULT "Normal",
PRIMARY KEY (ClaveMateria,MatriculaAlumno),
INDEX fk_ClaveMateria_MateriaAlumno_i (ClaveMateria ASC),
INDEX fk_MatriculaAlumno_MateriaAlumno_i (MatriculaAlumno ASC),
CONSTRAINT fk_ClaveMateria_MateriaAlumno_f
FOREIGN KEY (ClaveMateria)
REFERENCES MiniSIIA.curso (ClaveMateria)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_MatriculaAlumno_MateriaAlumno_f
FOREIGN KEY (MatriculaAlumno)
REFERENCES MiniSIIA.Alumno (MatriculaAlumno)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table CalifParcial(
CalifParcial INTEGER not null auto_increment,
ClaveMateria INTEGER not null,
fechaCreacion datetime default NOW(),
NoParcial INTEGER not null,
PRIMARY KEY (CalifParcial,ClaveMateria),
INDEX fk_ClaveMateria_CalifParcial_i (ClaveMateria ASC),
CONSTRAINT fk_ClaveMateria_CalifParcial_f
FOREIGN KEY (ClaveMateria)
REFERENCES MiniSIIA.curso (ClaveMateria)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table AlumnosCalifParcial(
CalifParcial INTEGER not null,
MatriculaAlumno INTEGER not null,
Calificacion INTEGER not null,
Estado boolean DEFAULT true,
Faltas INTEGER default 0,
Razon VARCHAR(18) DEFAULT "Aprovado",
Acentada boolean DEFAULT false
FechaCreacion DATETIME default NOW(),
PRIMARY KEY (CalifParcial,MatriculaAlumno),
INDEX fk_CalifParcial_AlumnosCalifParcial_i (CalifParcial ASC),
INDEX fk_MatriculaAlumno_AlumnosCalifParcial_i (MatriculaAlumno ASC),
CONSTRAINT fk_CalifParcial_AlumnosCalifParcial_f
FOREIGN KEY (CalifParcial)
REFERENCES MiniSIIA.CalifParcial (CalifParcial)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_MatriculaAlumno_AlumnosCalifParcial_f
FOREIGN KEY (MatriculaAlumno)
REFERENCES MiniSIIA.Alumno (MatriculaAlumno)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table CalifFinal(
CalifFinal INTEGER not null auto_increment,
MatriculaAlumno INTEGER not null,
ClaveMateria INTEGER not null,
Calificacion INTEGER not null,
Estado boolean DEFAULT true,
Razon VARCHAR(18) DEFAULT "Aprovado",
Acentada boolean DEFAULT false,
FechaCreacion datetime default NOW(),
PRIMARY KEY (CalifFinal,MatriculaAlumno,ClaveMateria),
INDEX fk_ClaveMateria_CalifFinal_i (ClaveMateria ASC),
INDEX fk_MatriculaAlumno_CalifFinal_i (MatriculaAlumno ASC),
CONSTRAINT fk_ClaveMateria_ACalifFinal_f
FOREIGN KEY (ClaveMateria)
REFERENCES MiniSIIA.curso (ClaveMateria)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_MatriculaAlumno_CalifFinal_f
FOREIGN KEY (MatriculaAlumno)
REFERENCES MiniSIIA.Alumno (MatriculaAlumno)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table ClavesModParcial(
ClaveModParcial varchar(20) not null,    
CalifParcial INTEGER not null,
MatriculaAlumno INTEGER not null,
MatriculaMaestro integer not null,
Descripcion VARCHAR(80) not null,
Activo boolean DEFAULT true,
FechaCreacion datetime default NOW(),
id  int(10) unsigned NOT NULL,
PRIMARY KEY (ClaveModParcial,CalifParcial,MatriculaAlumno,MatriculaMaestro,id),
INDEX fk_usuario_ClavesMod_i (id ASC),
INDEX fk_MatriculaMaestro_ClavesMod_i (MatriculaMaestro ASC),
CONSTRAINT fk_CalifParcial_ClavesMod_f
FOREIGN KEY (CalifParcial)
REFERENCES MiniSIIA.CalifParcial (CalifParcial)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_MatriculaAlumno_ClavesMod_f
FOREIGN KEY (MatriculaAlumno)
REFERENCES MiniSIIA.Alumno (MatriculaAlumno)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_matriculaMaestro_ClavesMod_f
FOREIGN KEY (MatriculaMaestro)
REFERENCES MiniSIIA.Maestro (MatriculaMaestro)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_usuario_ClavesMod_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table ModCalifParcial(
ClaveModParcial varchar(20) not null,    
CalifParcial INTEGER not null,
CalifAnterior INTEGER not null,
CalifNueva integer not null,
FechaCreacion date default NOW(),
PRIMARY KEY (ClaveModParcial,CalifParcial),
INDEX fk_ClaveModParcial_ModCalifParcial_i (ClaveModParcial ASC),
INDEX fk_CalifParcial_ModCalifParcial_i (CalifParcial ASC),
CONSTRAINT fk_ClaveModParcial_ModCalifParcial_f
FOREIGN KEY (ClaveModParcial)
REFERENCES MiniSIIA.ClavesModParcial (ClaveModParcial)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_CalifParcial_ModCalifParcial_f
FOREIGN KEY (CalifParcial)
REFERENCES MiniSIIA.CalifParcial (CalifParcial)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table ClavesModFinal(
ClaveModFinal varchar(20) not null,    
ClaveMateria INTEGER not null,
MatriculaAlumno INTEGER not null,
MatriculaMaestro integer not null,
Descripcion VARCHAR(80) not null,
Activo boolean DEFAULT true,
FechaCreacion datetime default NOW(),
id  int(10) unsigned NOT NULL,
PRIMARY KEY (ClaveModFinal,ClaveMateria,MatriculaAlumno,MatriculaMaestro,id),
INDEX fk_usuario_ClavesModFinal_i (id ASC),
INDEX fk_MatriculaMaestro_ClavesModFinal_i (MatriculaMaestro ASC),
CONSTRAINT fk_ClaveMateria_ClavesModFinal_f
FOREIGN KEY (ClaveMateria)
REFERENCES MiniSIIA.Curso (ClaveMateria)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_MatriculaAlumno_ClavesModFinal_f
FOREIGN KEY (MatriculaAlumno)
REFERENCES MiniSIIA.Alumno (MatriculaAlumno)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_matriculaMaestro_ClavesModFinal_f
FOREIGN KEY (MatriculaMaestro)
REFERENCES MiniSIIA.Maestro (MatriculaMaestro)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_usuario_ClavesModFinal_f
FOREIGN KEY (id)
REFERENCES MiniSIIA.users (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -------------------------------------------------------------------------
create table ModCalifFinal(
ClaveModParcial varchar(20) not null,    
CalifFinal INTEGER not null,
CalifAnterior INTEGER not null,
CalifNueva integer not null,
FechaCreacion datetime default NOW(),
PRIMARY KEY (ClaveModParcial,CalifFinal),
INDEX fk_ClaveModParcial_ModCalifFinal_i (ClaveModParcial ASC),
INDEX fk_CalifFinal_ModCalifFinal_i (CalifFinal ASC),
CONSTRAINT fk_ClaveModParcial_ModCalifFinal_f
FOREIGN KEY (ClaveModParcial)
REFERENCES MiniSIIA.ClavesModParcial (ClaveModParcial)
ON DELETE NO ACTION
ON UPDATE NO ACTION,
CONSTRAINT fk_CalifFinal_ModCalifFinal_f
FOREIGN KEY (CalifFinal)
REFERENCES MiniSIIA.CalifFinal (CalifFinal)
ON DELETE NO ACTION
ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

------------------------------------------------------------------------
