<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class materiaController extends Controller
{
    public function store(Request $request)
    {
        $Materia = $request->input('materia');
        $carrera = $request->input('carrera');
        $unidades = $request->input('unidades');
        $usuario = Auth::id();

        $insertado = DB::insert('insert into materia (Nombre, Carrera, Unidades, id, FechaCreacion)
            values(?,?,?,?,NOW());',
            [$Materia,$carrera,$unidades,$usuario]);
        
        if($insertado){
            $resultado = "La materia: << ".$Materia." >> ha sido dada de alta exitósamente";
            return view('/registro/materia')->with('resultado', $resultado);
        }else{
            $resultado = "Materia no registrada";
            return view('/registro/materia')->with('resultado', $resultado);
        }    
    }

    public function datos(){
        if(Auth::check()){
            $id= Auth::id();
            $rol= '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();            
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol = 'Administrador'){
                $resultado= null;
                $consulta=  DB::table('materia')->select(DB::raw("clave, nombre, carrera, unidades, id"))
                    ->paginate(10);
                return view('eliminacion/eliminarMateria')->with('resultado', $resultado)->with('materias', $consulta);
            }else{
                return redirect('home');
            }
        }else{
            return redirect('home');
        }        
    }

    public function editar($clave)
    {
        if(Auth::check()){
            $id= Auth::id();
            $rol= '';
            $consultaRol= DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol= $c->Rol;
            }
            if($rol == 'Administrador'){
                $resultado=null;
                $consulta = DB::select('SELECT clave, nombre, carrera, unidades, id from materia where clave = ?;',[$clave]);
                return view('/modificacion/modificarMateria')->with('datos',$consulta)->with('resultado',$resultado);
            }else{
                return redirect('home?');
            }
        }else{
            return redirect('home');
        }
        
    }

    public function modificar(Request $request)
    {
        $clave = $request->input('clave');
        $nombre = $request->input('nombre');
        $carrera = $request->input('carrera');
        $unidades = $request->input('unidades');
        $idUsuario = $request->input('id');
        
        $consulta = DB::update("UPDATE materia set nombre= ?, carrera = ?, unidades = ? where clave = ?;",
        [$nombre,$carrera,$unidades,$clave]);

        if($consulta){
            $resultado = "La materia con la clave".$clave." a sido actualizada";
            $consulta2 = DB::select('SELECT clave, nombre,carrera,unidades,id from materia where clave = ?;',[$clave]);
            return view('/modificacion/modificarMateria')->with('datos',$consulta2)->with('resultado',$resultado);
        }else{
            $resultado = "La materia no ha podido ser actualizada";
            $consulta2 = DB::select('SELECT clave, nombre,carrera,unidades,id from materia where clave = ?;',[$clave]);
            return view('/modificacion/modificarMateria')->with('datos',$consulta2)->with('resultado',$resultado);
        }
        
    }

    public function eliminar($clave)
    {
        if(Auth::check()){            
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $consulta = DB::table('materia')->where('clave','=',$clave)->delete();
                if($consulta){
                    
                    $resultado = "La Materia con la clave".$clave." a sido eliminada";    
                    $consulta2=  DB::table('materia')->select(DB::raw("clave, nombre, carrera, unidades, id"))
                    ->paginate(10);
                    return view('/eliminacion/eliminarMateria')->with('materias',$consulta2)->with('resultado',$resultado);
                }else{
                    $resultado = "El materia no ha sido eliminada";
                    $consulta2=  DB::table('materia')->select(DB::raw("clave, nombre, carrera, unidades, id"))
                    ->paginate(10);
                    return view('/eliminacion/eliminarMateria')->with('materias',$consulta2)->with('resultado',$resultado);
                }
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
}
