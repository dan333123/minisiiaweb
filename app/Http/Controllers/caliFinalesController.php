<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class caliFinalesController extends Controller
{
    public function cursosActuales(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $CodigoSemestre = 0;
            $matriculaMaestro = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matriculaMaestro =$c->Matricula;
            }
           
            if($rol=='Maestro'){
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();

                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('curso')
                ->join('materia','materia.clave', '=', 'curso.clave')
                ->select('ClaveMateria','Grupo','Salon','Nombre','Unidades')
                ->where('CodigoSemestre','=',$CodigoSemestre)
                ->where('MatriculaMaestro','=',$matriculaMaestro)->paginate(10);
                return view('/Interfaz/maestrosCursosF')->with('curso', $consulta);

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }    
    }

    public function cursoCalificar($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
           
            if($rol=='Maestro'){
                $alumnos = DB::table('materiaalumno')->join('alumno','alumno.MatriculaAlumno','=','materiaalumno.MatriculaAlumno')
                ->select(DB::raw("materiaalumno.MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre"))
                ->where('ClaveMateria','=',$claveMateria)->get();
                
                return view('/Interfaz/maestroCursoFinalAlumno')->with('claveMateria',$claveMateria)->with('alumno',$alumnos);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }        
    }
    
    public function cursoCalificarAlumno($claveMateria,$matriculaAlumno){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $com = 0;
            $califFinal = 0;
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();

            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Maestro'){
                $datos = DB::table('califfinal')->select('Calificacion','Estado','Razon','Acentada')
                ->where('MatriculaAlumno','=',$matriculaAlumno)->where('ClaveMateria','=',$claveMateria)->get();
                foreach($datos as $c){
                    $com = 1;
                }
                if($com==1){
                    return view('/registro/maestroCursoCalificarFinal')
                    ->with('alumno',$datos)->with('MatriculaAlumno',$matriculaAlumno)
                    ->with('claveMateria',$claveMateria)->with('resultado',' ');
                }else{
                    return view('/registro/maestroCursoCalificarFinalS')
                    ->with('MatriculaAlumno',$matriculaAlumno)
                    ->with('claveMateria',$claveMateria)->with('resultado',' ');
                }
               
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }        
    }     

    public function store(Request $request){
        $MatriculaAlumno = $request->input('MatriculaAlumno');
        $Calificacion = $request->input('Calificacion');
        $Estado = $request->input('aprovado');
        $razon = $request->input('razon');
        $acentado = $request->input('acentado');
        $claveMateria = $request->input('claveMateria');

        $insertar = DB::table('califfinal')
        ->insert(['MatriculaAlumno' => $MatriculaAlumno,'ClaveMateria' => $claveMateria,
        'Calificacion' => $Calificacion,'Estado' => $Estado,'Razon' => $razon,'Acentada' => $acentado]);
        if($insertar){
            $datos = DB::table('califfinal')->select('Calificacion','Estado','Razon','Acentada')
                ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('ClaveMateria','=',$claveMateria)->get();
                return view('/registro/maestroCursoCalificarFinal')
                ->with('alumno',$datos)->with('MatriculaAlumno',$MatriculaAlumno)
                ->with('claveMateria',$claveMateria)->with('resultado','Alumno Calificado');
        }else{
            return view('/registro/maestroCursoCalificarFinalS')
                    ->with('MatriculaAlumno',$MatriculaAlumno)
                    ->with('claveMateria',$claveMateria)->with('resultado','No se pudo calificar');
        }
    }

    public function actualizar(Request $request){
        $MatriculaAlumno = $request->input('MatriculaAlumno');
        $Calificacion = $request->input('Calificacion');
        $Estado = $request->input('aprovado');
        $razon = $request->input('razon');
        $acentado = $request->input('acentado');
        $claveMateria = $request->input('claveMateria');
        $califParcial = 0;

        $actualizar = DB::table('califfinal')
        ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('ClaveMateria','=',$claveMateria)
        ->update(['Calificacion' => $Calificacion,'Estado' => $Estado,
         'Razon' => $razon,'Acentada' => $acentado]);
         if($actualizar){
            $datos = DB::table('califfinal')->select('Calificacion','Estado','Razon','Acentada')
            ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('ClaveMateria','=',$claveMateria)->get();
            return view('/registro/maestroCursoCalificarFinal')
            ->with('alumno',$datos)->with('MatriculaAlumno',$MatriculaAlumno)
            ->with('claveMateria',$claveMateria)->with('resultado','Datos actualizados');
         }else{
            $datos = DB::table('califfinal')->select('Calificacion','Estado','Razon','Acentada')
            ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('ClaveMateria','=',$claveMateria)->get();
            return view('/registro/maestroCursoCalificarFinal')
            ->with('alumno',$datos)->with('MatriculaAlumno',$MatriculaAlumno)
            ->with('claveMateria',$claveMateria)->with('resultado','No se pudo actulizar los datos');
         }
    }
}
