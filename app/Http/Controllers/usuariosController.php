<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class usuariosController extends Controller
{
    public function usuarios(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('users')->join('roles', 'users.id', '=', 'roles.id')
                ->select('users.id','password','Rol','email')->orderBy('email')->paginate(10);
                return view('Busquedas/usuarios')->with('usuarios',$consulta);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function modificarE($usuario){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $resultado = '';
                $consulta = DB::table('users')->join('roles', 'users.id', '=', 'roles.id')
                ->select('users.id','password','Rol','email')->where('users.id','=',$usuario)->get();
                return view('modificacion/modificarUsuarios')->with('datos',$consulta)->with('resultado',$resultado);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function modificar(Request $request){
        $usuario = $request->input('usuario2');
        $contra =  bcrypt($request->input('contra'));
        $consulta = DB::table('users')->where('id','=',$usuario)
        ->update(['password' => $contra]);

        $consulta2 = DB::table('users')->join('roles', 'users.id', '=', 'roles.id')
        ->select('users.id','password','Rol','email')->where('users.id','=',$usuario)->get();
        if($consulta){
            $resultado = 'El usuario; '.$usuario.' ha sido modificado';
            return view('modificacion/modificarUsuarios')->with('datos',$consulta2)->with('resultado',$resultado);
        }else{
            $resultado = 'El usuario no ha podido ser modificado';
            return view('modificacion/modificarUsuarios')->with('datos',$consulta2)->with('resultado',$resultado);
        }
    }

    public function eliminar($usuario){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('users')->where('id','=',$usuario)
                ->delete();
                $consulta2 = DB::table('roles')->where('id','=',$usuario)
                ->delete();
                return redirect('/extras/usuarios');
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
}
