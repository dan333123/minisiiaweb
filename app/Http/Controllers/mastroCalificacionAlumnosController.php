<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class mastroCalificacionAlumnosController extends Controller
{
    public function alumnosMaestro(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $CodigoSemestre = 0;
            $matriculaMaestro = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matriculaMaestro =$c->Matricula;
            }
           
            if($rol=='Maestro'){
                $consulta = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                $codigoSemestre = 0;
                $contador = 0;
                foreach($consulta as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                    $contador = 1;
                }
                if($contador==1){
                    $curso = DB::select("select curso.ClaveMateria, materia.nombre, grupo, salon, materiaalumno.matriculaAlumno, concat(alumno.nombre,' ',apellidoP,' ',apellidoM) as nombreC from curso 
                    inner join materia on curso.clave = materia.clave
                    inner join materiaalumno on materiaalumno.clavemateria = curso.clavemateria
                    inner join alumno on alumno.matriculaAlumno = materiaalumno.matriculaalumno
                     where CodigoSemestre = ? and materiaalumno.matriculaAlumno in ( select matriculaAlumno
                    from materiaalumno
                    inner join curso on curso.clavemateria = materiaalumno.clavemateria
                    where matriculamaestro = ?)
                     order by materia.nombre;",[$codigoSemestre,$matriculaMaestro]);
                    return view('Interfaz/maestroAlumnosActuales')->with('curso',$curso);
                }else{
                    return view('/home');
                }

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }      
    }

    public function califAlumno($claveMateria,$matriculaAlumno){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $CodigoSemestre = 0;
            $matriculaMaestro = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matriculaMaestro =$c->Matricula;
            }
           
            if($rol=='Maestro'){
                $consulta = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                $codigoSemestre = 0;
                $contador = 0;
                foreach($consulta as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                    $contador = 1;
                }
                if($contador==1){
                    $datos = DB::table('alumnosCalifParcial')
                    ->join('califParcial','alumnosCalifParcial.CalifParcial','=','califParcial.CalifParcial')
                    ->join('curso','curso.ClaveMateria','=','califParcial.ClaveMateria')
                    ->select('MatriculaAlumno','Calificacion','Estado','Razon','noParcial')
                    ->where('curso.ClaveMateria','=',$claveMateria)
                    ->where('MatriculaAlumno','=',$matriculaAlumno)
                    ->where('curso.MatriculaMaestro','=',$matriculaMaestro)
                    ->get();
                    return view('Reportes/maestroReporteAlumno')->with('alumnos',$datos);


                }else{
                    return view('/home');
                }

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }     
    }

    public function cursosActuales(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $CodigoSemestre = 0;
            $matriculaMaestro = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matriculaMaestro =$c->Matricula;
            }
           
            if($rol=='Maestro'){
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();

                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('curso')
                ->join('materia','materia.clave', '=', 'curso.clave')
                ->select('ClaveMateria','Grupo','Salon','Nombre','Unidades')
                ->where('CodigoSemestre','=',$CodigoSemestre)
                ->where('MatriculaMaestro','=',$matriculaMaestro)->paginate(10);
                return view('/Interfaz/maestroCursos')->with('curso', $consulta);

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }    
    }

    public function alumnos($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $CodigoSemestre = 0;
            $matriculaMaestro = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matriculaMaestro =$c->Matricula;
            }
           
            if($rol=='Maestro'){
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();

                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('curso')
                ->join('materiaalumno','materiaalumno.ClaveMateria','=','curso.ClaveMateria')
                ->join('alumno','alumno.MatriculaAlumno','=','materiaalumno.MatriculaAlumno')
                ->select(DB::raw("CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre ,alumno.MatriculaAlumno,SemestreAlumno,Curso"))
                ->where('CodigoSemestre','=',$CodigoSemestre)
                ->where('MatriculaMaestro','=',$matriculaMaestro)
                ->where('materiaalumno.ClaveMateria','=',$claveMateria)
                ->paginate(20);
                return view('Reportes/maestroReporteAlumnos')->with('alumnos', $consulta);

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

}
