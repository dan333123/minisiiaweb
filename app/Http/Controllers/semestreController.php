<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class semestreController extends Controller
{
    public function semestre(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('semestre')->select('CodigoSemestre','Campus','FechaInicio','FechaFin','CalifAprovatoria','NoParciales')->where('Activo','=',1)->get();
                $registros = 0;
                $CodigoSemestre = 0;
                foreach($consulta as $c){
                $registros = 1;
                }

                if($registros == 1){
                    
                    foreach($consulta as $c){
                        $CodigoSemestre = $c->CodigoSemestre;
                    }

                    $consulta2 = DB::select('SELECT CodigoSemestre,NoParcial,FechaInicio,FechaFin from parcial where CodigoSemestre = ?',[$CodigoSemestre]);
                    $registros=0;
                    foreach($consulta2 as $c){
                        $registros = 1;
                        }
                        if($registros == 1){
                            return view('Interfaz/semestreInterfaz')->with('semestre',$consulta)->with('parcial',$consulta2);
                        }else{
                            $consulta3 = DB::table('semestre')->select('NoParciales')->where('CodigoSemestre','=',$CodigoSemestre)->first();
                            return view('registro/parciales')->with('resultado', $consulta3->NoParciales)->with('codigoSemestre',$CodigoSemestre);
                        }
                }else{
                    return view('registro/semestre');
                }
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function store(Request $request){
        $Campus = $request->input('Campus');
        $NoParciales = $request->input('NoParciales');
        $CalifAprovatoria = $request->input('CalifAprovatoria');
        $FechaInicio = $request->input('FechaInicio');
        $FechaFin=  $request->input('FechaFin');
        $usuario = Auth::id();

        $insertado = DB::insert('insert into semestre(CodigoSemestre,Campus, NoParciales, CalifAprovatoria, FechaInicio,
        FechaFin,FechaCreacion ,Activo,id) values(null,?,?,?,?,?,NOW(),true,?);',
            [$Campus,$NoParciales,$CalifAprovatoria,$FechaInicio,$FechaFin,$usuario]);

        if($insertado){
            $consulta2 = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
            foreach($consulta2 as $c){
                $CodigoSemestre = $c->CodigoSemestre;
            }
            return view('registro/parciales')->with('resultado', $NoParciales)->with('codigoSemestre',$CodigoSemestre);
        }else{
            return('No insertado');
        }    
    }

    public function prueba(){
        $resultado = 3;
        return view('registro/parciales')->with('resultado', $resultado);
    }

    public function storeParcial(Request $request){
        $noParciales = $request->input('parciales');
        $codigoSemestre  = $request->input('codigoSemestre');
        $CodigoSemestre = 0;

        for($i=0;$i<$noParciales;$i++){
            $noParcial = $i+1;
            $FechaInicio = $request->input('FechaInicio'.$i);
            $FechaFin = $request->input('FechaFin'.$i);
            $consulta = DB::insert('insert into parcial(CodigoSemestre,NoParcial, FechaInicio, FechaFin) values(?,?,?,?);',
                [$codigoSemestre,$noParcial,$FechaInicio,$FechaFin]);
        }
        $consulta2 = DB::table('semestre')->select('CodigoSemestre','Campus','FechaInicio','FechaFin','CalifAprovatoria','NoParciales')->where('Activo','=',1)->get();
        foreach($consulta2 as $c){
            $CodigoSemestre = $c->CodigoSemestre;
        }
        
        $consulta3 = DB::select('SELECT CodigoSemestre,NoParcial,FechaInicio,FechaFin from parcial where CodigoSemestre = ?',[$CodigoSemestre]);
        return view('Interfaz/semestreInterfaz')->with('semestre',$consulta2)->with('parcial',$consulta3);
    }

    public function eliminar($codigoSemestre){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('semestre')
                ->where('CodigoSemestre','=',$codigoSemestre)
                ->update(['Activo' => false]);
                return view('home');
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
    
}
