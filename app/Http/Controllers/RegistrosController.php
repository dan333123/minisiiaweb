<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request;

class RegistrosController extends Controller
{
    public function registrarMaestro()
    {
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consulta = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consulta as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){   
            $resultado=null;
             return view('registro.maestro',['resultado' => $resultado,'color' => 1]);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
        
    }

    public function registrarAlumno()
    {
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consulta = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consulta as $c){
                $rol = $c->Rol;
            }            
            if($rol=='Administrador'){    
        $resultado=null;
        return view('registro.alumno',['resultado' => $resultado,'color' => 1]);
            }else{
                return redirect('/home');
    }
    }else{
        return redirect('/home');
    }
    
    }
    
    public function registrarMateria()
    {
        if(Auth::check()){
        $id = Auth::id();
        $rol = '';
        $consulta = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
        foreach($consulta as $c){
            $rol = $c->Rol;
        }
        if($rol=='Administrador'){
            $resultado= null;
             return view('registro.materia',['resultado' => $resultado, 'color' => 1]);
        }else{
            return redirect('/home');
        }
        
    }else{
        return redirect('/home');
    }
    }
}
