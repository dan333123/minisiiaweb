<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //uso de la calse DB para poder modificar nuestra base
use Illuminate\Support\Facades\Auth;
use app\Maestro;
use PDF;
class maestroController extends Controller
{
    public function store(Request $request){
        $MatriculaMaestro = $request->input('matricula');
        $apellidoP = $request->input('apellidoP');
        $apellidoM = $request->input('apellidoM');
        $nombre = $request->input('nombre');
        $departamento = $request->input('departamento');
        $telefono = $request->input('telefono');
        $email = $request->input('email');
        if($request->input('genero'=="Masculino")){
            $genero = true;
        }else{
            $genero = false;
        }
        $usuario = Auth::id();

        $insertado = DB::insert('insert into maestro (MatriculaMaestro, nombre, apellidoP, apellidoM,
            Genero, Departamento, Correo, telefono, Activo, fechaCreacion, id)
            values(?,?,?,?,?,?,?,?,true,NOW(),?);',
            [$MatriculaMaestro,$nombre,$apellidoP,$apellidoM,$genero,$departamento,$email,$telefono,$usuario]);

        if($insertado){
            $pass = bcrypt($MatriculaMaestro);
                
            $usuario = DB::table('users')->insertGetId(['name' => 'maestro', 'email' => $email,'username' => $MatriculaMaestro,'password' => $pass]);
            $rol = DB::table('roles')->insert(['id' => $usuario, 'Rol' => 'Maestro','Matricula' => $MatriculaMaestro]);
            $resultado = "Maestro con matricula: ".$MatriculaMaestro." a sido agregado";
            return view('/registro/maestro')->with('resultado', $resultado);
        }else{
            $resultado = "Maestro no agregado";
            return view('/registro/maestro')->with('resultado', $resultado);
        }
        

    }

    public function datos(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $resultado=null;
                $consulta = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Departamento,correo,telefono"))
                ->where('Activo', '=', 1)->paginate(10);
                //$consulta = DB::select("SELECT matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Departamento,correo,telefono FROM maestro where Activo = true;");
                return view('/eliminacion/eliminarMaestro')->with('maestros',$consulta)->with('resultado',$resultado);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function editar($matricula){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $resultado=null;
                $consulta = DB::select('SELECT matriculaMaestro, nombre,apellidoP,apellidoM,genero,departamento,correo,telefono from maestro where Activo = true and  matriculaMaestro = ?;',[$matricula]);
                //$consulta2 = DB::table('maestro')->select('matriculaMaestro', 'nombre','apellidoP','apellidoM','genero','departamento','correo','telefono')
                //->where('Activo', '=', true)
                //->where('matriculaMaestro', '=', $matricula)->get();
                return view('/modificacion/modificarMaestro')->with('datos',$consulta)->with('resultado',$resultado);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function modificar(Request $request){
        $MatriculaMaestro = $request->input('matricula');
        $MatriculaMaestroAnterior = $request->input('matriculaAnterior');
        $apellidoP = $request->input('apellidoP');
        $apellidoM = $request->input('apellidoM');
        $nombre = $request->input('nombre');
        $departamento = $request->input('departamento');
        $telefono = $request->input('telefono');
        $email = $request->input('email');
        if($request->input('genero'=="Masculino")){
            $genero = true;
        }else{
            $genero = false;
        }
        
        $consulta = DB::update("UPDATE maestro set matriculaMaestro = ?, nombre= ?, apellidoP = ?, apellidoM = ?, genero = ?, departamento = ?, correo = ?, telefono = ? where matriculaMaestro = ?;",
        [$MatriculaMaestro,$nombre,$apellidoP,$apellidoM,$genero,$departamento,$email,$telefono,$MatriculaMaestroAnterior]);

        if($consulta){
            $resultado = "Maestro con matricula: ".$MatriculaMaestroAnterior." a sido actualizado";
            $consulta2 = DB::select('SELECT matriculaMaestro, nombre,apellidoP,apellidoM,genero,departamento,correo,telefono from maestro where Activo = true and  matriculaMaestro = ?;',[$MatriculaMaestro]);
            return view('/modificacion/modificarMaestro')->with('datos',$consulta2)->with('resultado',$resultado);
        }else{
            $resultado = "El maestro no ha podido ser eliminado";
            $consulta2 = DB::select('SELECT matriculaMaestro, nombre,apellidoP,apellidoM,genero,departamento,correo,telefono from maestro where Activo = true and  matriculaMaestro = ?;',[$MatriculaMaestroAnterior]);
            return view('/modificacion/modificarMaestro')->with('datos',$consulta2)->with('resultado',$resultado);
        }

    }

    public function eliminar($matricula){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $consulta = DB::update('UPDATE maestro set Activo = 0 where matriculaMaestro = ?;',[$matricula]);
                if($consulta){
                    $resultado = "Maestro con matricula: ".$matricula." a sido eliminado";
                    $consulta2 = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Departamento,correo,telefono"))
                    ->where('Activo', '=', 1)->paginate(10);
                    return view('/eliminacion/eliminarMaestro')->with('maestros',$consulta2)->with('resultado',$resultado);
                }else{
                    $resultado = "El maestro no ha podido ser eliminado";
                    $consulta2 = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Departamento,correo,telefono"))
                    ->where('Activo', '=', 1)->paginate(10);
                    return view('/eliminacion/eliminarMaestro')->with('maestros',$consulta2)->with('resultado',$resultado);
                }
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function buscar(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $consulta = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Departamento,correo,telefono,genero"))
                ->where('Activo', '=', 1)->paginate(10);
                return view('/Busquedas/busquedaMaestro')->with('maestros',$consulta);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function buscarParcial(Request $request){
        $MatriculaMaestro = $request->input('matricula');
        $consulta = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Departamento,correo,telefono,genero"))
        ->where('MatriculaMaestro' ,'LIKE','%'.$MatriculaMaestro.'%')
        ->where('Activo', '=', 1)->paginate(10);
        return view('/Busquedas/busquedaMaestro')->with('maestros',$consulta);
    }

    /* Ver calificaciones */
    public function verCalificaciones($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            $consultaMatricula = DB::table('roles')->select('Matricula')->where('id','=',$id)->get();
            foreach($consultaMatricula as $matri){
                $matricula=$matri->Matricula;
            }
            if($rol=='Maestro'){ 
                $noAlumnos = DB::table('maestro')
                ->join('curso','curso.MatriculaMaestro','=','maestro.MatriculaMaestro')
                ->join('materiaalumno','materiaalumno.ClaveMateria','=','curso.ClaveMateria')
                ->join('alumno','alumno.MatriculaAlumno','materiaalumno.MatriculaAlumno')
                ->select('alumno.nombre')
                ->where('maestro.MatriculaMaestro','=',$matricula)
                ->where('curso.ClaveMateria','=',$claveMateria)
                ->get();
                $calificaciones= DB::table('maestro')
                ->join('curso','curso.MatriculaMaestro','=','maestro.MatriculaMaestro')
                ->join('califParcial','califParcial.ClaveMateria','=','curso.ClaveMateria')
                ->join('alumnoscalifparcial','alumnoscalifparcial.CalifParcial','=','califParcial.CalifParcial')
                ->join('alumno','alumno.MatriculaAlumno','alumnoscalifparcial.MatriculaAlumno')
                ->select('alumno.nombre','califParcial.NoParcial','alumnoscalifparcial.calificacion','alumnoscalifparcial.razon')
                ->where('maestro.MatriculaMaestro','=',$matricula)
                ->where('curso.ClaveMateria','=',$claveMateria)
                ->get();
                return view('/maestros/calificaciones')->with('noAlumnos',$noAlumnos)->with('calificaciones',$calificaciones);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
}
