<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class cursoAlumnoController extends Controller
{
    public function cursosActuales(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $consulta = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                $codigoSemestre = 0;
                $contador = 0;
                foreach($consulta as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                    $contador = 1;
                }
                if($contador==1){
                    $consulta2 = DB::table('materia')
                    ->join('curso', 'materia.clave', '=', 'curso.clave')
                    ->join('maestro','curso.MatriculaMaestro','=','maestro.MatriculaMaestro')
                    ->select(DB::RAW("materia.nombre, materia.Carrera,ClaveMateria,CONCAT(maestro.nombre,' ',apellidoP,' ',apellidoM) as nombreM,Grupo,Salon"))
                    ->where('CodigoSemestre','=', $codigoSemestre)->distinct()->orderBy('materia.nombre','asc')->orderBy('Grupo','asc')->paginate(10);
                    
                    return view('Interfaz/cursosAlumnos')->with('materia',$consulta2);
                }else{
                    return view('registro/semestre');
                }


            }else{
                return redirect('/home');
            }
        }else{
             return redirect('/home');
    }    
    }

    public function agregarAlumnos($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 

                $consulta = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                $codigoSemestre = 0;
                $contador = 0;
                foreach($consulta as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                    $contador = 1;
                }
                if($contador==1){
                    $curso = DB::table('curso')->join('materia','curso.clave','=','materia.clave')
                    ->select('carrera')->where('ClaveMateria','=',$claveMateria)
                    ->get();
                    foreach($curso as $c){
                        $carrera = $c->carrera;
                    }

                    /*$a = DB::table('materiaAlumno')->select(DB::raw('MatriculaAlumno'))
                    ->whereRaw('ClaveMateria = ?',[$claveMateria])->get();

                    $ejem = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera"))
                    ->where('Activo','=',1)->where('Carrera','=',$carrera)->whereNotIn('MatriculaAlumno',$a)->paginate(10);*/
                    $alumnos = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera"))
                    ->where('Activo','=',1)->where('Carrera','=',$carrera)->orderBy('nombre','asc')->paginate(10);

                    return view('registro/cursoAlumno')->with('alumno',$alumnos)->with('claveMateria',$claveMateria)->with('resultado','');

                }else{
                    return view('registro/semestre');
                }    

            }else{
                return redirect('/home');
            }
        }else{
             return redirect('/home');
    }        
    }

    public function store(Request $request){
        $matriculaAlumno = $request->input('matricula');
        $claveMateria = $request->input('claveMateria');
        $semestreAlumno = $request->input('semestreA');
        $curso = $request->input('curso');
        $con =0;
        $prueba = DB::table('materiaAlumno')->select('ClaveMateria')->where('claveMateria','=',$claveMateria)
        ->where('MatriculaAlumno','=',$matriculaAlumno)->get();
        foreach($prueba as $c){
            $con=1;
        }
        if($con==1){
            return redirect()->action('cursoAlumnoController@agregarAlumnos', [$claveMateria]);
        }else{
            $consulta = DB::table('materiaAlumno')
            ->insert(['MatriculaAlumno' => $matriculaAlumno, 'ClaveMateria' => $claveMateria, 'SemestreAlumno' => $semestreAlumno,'Curso' => $curso]);
            if($consulta){
                return redirect()->action('cursoAlumnoController@agregarAlumnos', [$claveMateria]);
            }else{
                return redirect()->action('cursoAlumnoController@agregarAlumnos', [$claveMateria]);
            }
        }
        
    }
}
