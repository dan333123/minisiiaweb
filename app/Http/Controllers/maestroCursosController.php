<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class maestroCursosController extends Controller
{
        public function cursosActuales(){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }
               
                if($rol=='Maestro'){
                    $semestre = DB::table('semestre')
                    ->select('CodigoSemestre')
                    ->where('Activo','=',1)
                    ->get();

                    foreach($semestre as $c){
                        $CodigoSemestre = $c->CodigoSemestre;
                    }
                    $consulta = DB::table('curso')
                    ->join('materia','materia.clave', '=', 'curso.clave')
                    ->select('ClaveMateria','Grupo','Salon','Nombre','Unidades')
                    ->where('CodigoSemestre','=',$CodigoSemestre)
                    ->where('MatriculaMaestro','=',$matriculaMaestro)->paginate(10);
                    return view('/Interfaz/maestroCursosActuales')->with('curso', $consulta);

                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }    
        }

        public function cursosActualesC(){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }
               
                if($rol=='Maestro'){
                    $semestre = DB::table('semestre')
                    ->select('CodigoSemestre')
                    ->where('Activo','=',1)
                    ->get();

                    foreach($semestre as $c){
                        $CodigoSemestre = $c->CodigoSemestre;
                    }
                    $consulta = DB::table('curso')
                    ->join('materia','materia.clave', '=', 'curso.clave')
                    ->select('ClaveMateria','Grupo','Salon','Nombre','Unidades')
                    ->where('CodigoSemestre','=',$CodigoSemestre)
                    ->where('MatriculaMaestro','=',$matriculaMaestro)->paginate(10);
                    return view('/Interfaz/maestrosCursos')->with('curso', $consulta);

                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }    
        }
        public function cursosActualesT(){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }
               
                if($rol=='Maestro'){
                    $semestre = DB::table('semestre')
                    ->select('CodigoSemestre')
                    ->where('Activo','=',1)
                    ->get();

                    foreach($semestre as $c){
                        $CodigoSemestre = $c->CodigoSemestre;
                    }
                    $consulta = DB::table('curso')
                    ->join('materia','materia.clave', '=', 'curso.clave')
                    ->select('ClaveMateria','Grupo','Salon','Nombre','Unidades')
                    ->where('CodigoSemestre','=',$CodigoSemestre)
                    ->where('MatriculaMaestro','=',$matriculaMaestro)->paginate(10);
                    return view('/maestros/materias')->with('curso', $consulta);

                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }    
        }
        public function cursoCalificarParcial($claveMateria){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }
               
                if($rol=='Maestro'){
                    $consulta = DB::table('semestre')->select('NoParciales')->where('Activo','=',1)->get();
                    foreach($consulta as $c){
                        $parciales = $c->NoParciales;
                    }
                    return view('/Interfaz/maestroCursoParcial')->with('parciales',$parciales)->with('claveMateria',$claveMateria);
                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }
        }

        public function cursoCalificar($claveMateria,$parcial){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }

               
                if($rol=='Maestro'){
                    $alumnos = DB::table('materiaalumno')->join('alumno','alumno.MatriculaAlumno','=','materiaalumno.MatriculaAlumno')
                    ->select(DB::raw("materiaalumno.MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre"))
                    ->where('ClaveMateria','=',$claveMateria)->get();
                    return view('/Interfaz/maestroCursoParcialAlumnos')->with('claveMateria',$claveMateria)->with('parcial',$parcial)->with('alumno',$alumnos);
                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }        
        }

        public function cursoCalificarAlumno($claveMateria,$parcial,$matriculaAlumno){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $com = 0;
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $califParcial = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();

                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }
                if($rol=='Maestro'){
                    $conCalifParcial = DB::table('califparcial')->select('CalifParcial')
                    ->where('ClaveMateria','=',$claveMateria)->where('NoParcial','=',$parcial)->get();
                    foreach($conCalifParcial as $c){
                        $califParcial = $c->CalifParcial;
                     }
                    $datos = DB::table('alumnoscalifparcial')->select('Calificacion','Estado','Faltas','Razon','Acentada')
                    ->where('MatriculaAlumno','=',$matriculaAlumno)->where('CalifParcial','=',$califParcial)->get();
                    foreach($datos as $c){
                        $com = 1;
                    }
                    if($com==1){
                        return view('/registro/maestroCursoCalificar')
                        ->with('alumno',$datos)->with('MatriculaAlumno',$matriculaAlumno)
                        ->with('curso',$parcial)->with('claveMateria',$claveMateria)->with('resultado',' ');
                    }else{
                        return view('/registro/maestroCursoCalificarS')
                        ->with('MatriculaAlumno',$matriculaAlumno)
                        ->with('curso',$parcial)->with('claveMateria',$claveMateria)->with('resultado',' ');
                    }
                   
                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }        
        } 
        
        public function store(Request $request){
            $MatriculaAlumno = $request->input('MatriculaAlumno');
            $Calificacion = $request->input('Calificacion');
            $Estado = $request->input('aprovado');
            $razon = $request->input('razon');
            $Faltas = $request->input('Faltas');
            $acentado = $request->input('acentado');
            $parcial = $request->input('curso');
            $claveMateria = $request->input('claveMateria');
            $califParcial = 0;
            $conCalifParcial = DB::table('califparcial')->select('CalifParcial')
            ->where('ClaveMateria','=',$claveMateria)->where('NoParcial','=',$parcial)->get();
            foreach($conCalifParcial as $c){
                $califParcial = $c->CalifParcial;
            }
            $insertar = DB::table('alumnosCalifParcial')
            ->insert(['CalifParcial'=> $califParcial,
            'MatriculaAlumno' => $MatriculaAlumno,'Calificacion' => $Calificacion,
            'Estado' => $Estado, 'Faltas' => $Faltas,'Razon' => $razon,'Acentada' => $acentado]);
            if($insertar){
                $datos = DB::table('alumnoscalifparcial')->select('Calificacion','Estado','Faltas','Razon','Acentada')
                    ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('CalifParcial','=',$califParcial)->get();
                        return view('/registro/maestroCursoCalificar')
                        ->with('alumno',$datos)->with('MatriculaAlumno',$MatriculaAlumno)
                        ->with('curso',$parcial)->with('claveMateria',$claveMateria)->with('resultado','Alumno calificado ');
            }else{
                return view('/registro/maestroCursoCalificarS')
                        ->with('MatriculaAlumno',$MatriculaAlumno)
                        ->with('curso',$parcial)->with('claveMateria',$claveMateria)->with('resultado','No se pudo calificar ');
            }
        }

        public function actualizar(Request $request){
            $MatriculaAlumno = $request->input('MatriculaAlumno');
            $Calificacion = $request->input('Calificacion');
            $Estado = $request->input('aprovado');
            $razon = $request->input('razon');
            $Faltas = $request->input('Faltas');
            $acentado = $request->input('acentado');
            $parcial = $request->input('curso');
            $claveMateria = $request->input('claveMateria');
            $califParcial = 0;
            $conCalifParcial = DB::table('califparcial')->select('CalifParcial')
            ->where('ClaveMateria','=',$claveMateria)->where('NoParcial','=',$parcial)->get();
            foreach($conCalifParcial as $c){
                $califParcial = $c->CalifParcial;
            }

            $actualizar = DB::table('alumnoscalifparcial')
            ->where('CalifParcial','=',$califParcial)->where('MatriculaAlumno','=',$MatriculaAlumno)
            ->update(['Calificacion' => $Calificacion,'Estado' => $Estado,
             'Faltas' => $Faltas,'Razon' => $razon,'Acentada' => $acentado]);
             if($actualizar){
                $datos = DB::table('alumnoscalifparcial')->select('Calificacion','Estado','Faltas','Razon','Acentada')
                    ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('CalifParcial','=',$califParcial)->get();
                        return view('/registro/maestroCursoCalificar')
                        ->with('alumno',$datos)->with('MatriculaAlumno',$MatriculaAlumno)
                        ->with('curso',$parcial)->with('claveMateria',$claveMateria)->with('resultado','Datos actualizados');
             }else{
                $datos = DB::table('alumnoscalifparcial')->select('Calificacion','Estado','Faltas','Razon','Acentada')
                    ->where('MatriculaAlumno','=',$MatriculaAlumno)->where('CalifParcial','=',$parcial)->get();
                        return view('/registro/maestroCursoCalificar')
                        ->with('alumno',$datos)->with('MatriculaAlumno',$MatriculaAlumno)
                        ->with('curso',$parcial)->with('claveMateria',$claveMateria)->with('resultado','No se pudo actualizar los datos');
             }
            }
        
}
