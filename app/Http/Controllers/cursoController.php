<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class cursoController extends Controller
{

    public function materias($clave){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $carrera  = '';
            $nombreM = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('materia')->select('carrera','nombre')->where('clave','=',$clave)->get();
                foreach($consulta as $c){
                    $carrera = $c->carrera;
                    $nombreM = $c->nombre;
                }
                $consulta2 = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombreM"))
                ->where('Activo', '=', 1)->where('Departamento','=',$carrera)->orderBy('nombreM')->get();
                return view('/registro/curso')->with('nombre',$nombreM)->with('maestro',$consulta2)->with('clave',$clave)->with('resultado','');

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }    
    }

    public function materia(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('materia')->select('clave','nombre','carrera')->orderBy('carrera')->paginate(10);
                return view('/interfaz/cursos')->with('materia',$consulta);

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function store(Request $request){
        $clave = $request->input('clave');
        $grupo = $request->input('grupo');
        $salon = $request->input('salon');
        $matriculaMaestro = $request->input('maestro');
        $codigoSemestre = '';
        $id  = Auth::id();
        $noParciales = 0;

        $semestre = DB::table('semestre')->select('CodigoSemestre','NoParciales')->where('Activo','=',1)->get();
        foreach($semestre as $c){
            $codigoSemestre = $c->CodigoSemestre;
            $noParciales = $c->NoParciales;
        }
        $consulta = DB::table('curso')
        ->insert(['clave' => $clave,'grupo' => $grupo,'salon' => $salon, 'MatriculaMaestro' => $matriculaMaestro, 'codigoSemestre' => $codigoSemestre, 'id' => $id]);
        $idCurso = DB::select('select  ClaveMateria from curso order by ClaveMateria desc limit 1');
        foreach($idCurso as $c){
            $claveMateria = $c->ClaveMateria;
        }
        for($i=0;$i<$noParciales;$i++){
            $parciales = DB::table('califparcial')->insert(['ClaveMateria' => $claveMateria,'NoParcial' => ($i+1)]);
        }
            $consulta2 = DB::table('materia')->select('carrera','nombre')->where('clave','=',$clave)->get();
                foreach($consulta2 as $c){
                    $carrera = $c->carrera;
                    $nombreM = $c->nombre;
                }
                $consulta3 = DB::table('maestro')->select(DB::raw("matriculaMaestro, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombreM"))
                ->where('Activo', '=', 1)->where('Departamento','=',$carrera)->orderBy('nombreM')->get();
                return view('/registro/curso')->with('nombre',$nombreM)->with('maestro',$consulta3)->with('clave',$clave)->with('resultado','Curso agregado');
    }

    public function cursosActivos(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $codigoSemestre = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $semestre = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                foreach($semestre as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('curso')
                ->join('materia','materia.clave','=','curso.clave')
                ->join('maestro','maestro.MatriculaMaestro','=','curso.MatriculaMaestro')
                ->select(DB::raw("CONCAT(maestro.nombre,' ',apellidoP,' ',apellidoM) as nombreC, ClaveMateria,Grupo,Salon,materia.Nombre"))
                ->where('CodigoSemestre','=',$codigoSemestre)
                ->orderBy('curso.clavemateria','asc')
                ->paginate(10);
                return view('/eliminacion/eliminarCurso')->with('cursos',$consulta)->with('resultado','');

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }

    }

    public function eliminar($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $codigoSemestre = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $semestre = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                foreach($semestre as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                }
                $eliminar = DB::table('curso')->where('ClaveMateria','=',$claveMateria)->delete();
                $consulta = DB::table('curso')
                ->join('materia','materia.clave','=','curso.clave')
                ->join('maestro','maestro.MatriculaMaestro','=','curso.MatriculaMaestro')
                ->select(DB::raw("CONCAT(maestro.nombre,' ',apellidoP,' ',apellidoM) as nombreC, ClaveMateria,Grupo,Salon,materia.Nombre"))
                ->where('CodigoSemestre','=',$codigoSemestre)
                ->orderBy('curso.clavemateria','asc')
                ->paginate(10);
                return view('/eliminacion/eliminarCurso')->with('cursos',$consulta)->with('resultado','Curso Eliminado');

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
    
    public function alumnos($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $codigoSemestre = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $semestre = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                foreach($semestre as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('materiaalumno')
                ->join('curso','materiaalumno.claveMateria','=','curso.claveMateria')
                ->join('alumno','alumno.MatriculaAlumno','=','materiaalumno.MatriculaAlumno')
                ->select(DB::raw("CONCAT(alumno.nombre,' ',apellidoP,' ',apellidoM) as nombreC, alumno.MatriculaALumno,SemestreAlumno,Curso"))
                ->where('CodigoSemestre','=',$codigoSemestre)
                ->where('materiaalumno.claveMateria','=',$claveMateria)
                ->paginate(20);
                return view('/Reportes/cursosAsignados')->with('cursos',$consulta);

            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
}
