<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class reSeActualAdminController extends Controller
{
    public function cursos(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){  
                $consulta = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                $codigoSemestre = 0;
                $contador = 0;
                foreach($consulta as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                    $contador = 1;
                }
                if($contador==1){
                    $consulta2 = DB::table('materia')->join('curso', 'materia.clave', '=', 'curso.clave')
                    ->select('nombre','Carrera','materia.clave')
                    ->where('CodigoSemestre','=', $codigoSemestre)->distinct()->orderBy('Carrera','asc')->paginate(10);
                    
                    return view('Reportes/administradorReporteActual')->with('cursos',$consulta2);
                }else{
                    return view('registro/semestre');
                }
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function curso($curso){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('semestre')->select('CodigoSemestre')->where('Activo','=',1)->get();
                $codigoSemestre = 0;
                $contador = 0;
                foreach($consulta as $c){
                    $codigoSemestre = $c->CodigoSemestre;
                    $contador = 1;
                }
                if($contador==1){
                    $consulta2 = DB::table('curso')->join('maestro', 'curso.MatriculaMaestro', '=', 'maestro.MatriculaMaestro')
                    ->join('materia', 'materia.clave', '=', 'curso.clave')
                    ->select(DB::raw("ClaveMateria,CONCAT(maestro.nombre,' ',apellidoP,' ',apellidoM) as nombreCompleto"))
                    ->where('materia.clave','=', $curso)->orderBy('nombreCompleto','asc')->paginate(10);
                    
                    return view('Reportes/administradorReporteActualCurso')->with('cursos',$consulta2)->with('noCurso',$curso);
                }else{
                    return view('registro/semestre');
                }
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }
    public function cursoe($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('semestre')->select('NoParciales')->where('Activo','=',1)->get();
                foreach($consulta as $c){
                    $parciales = $c->NoParciales;
                }
                return view('/Reportes/administradorReporteParcial')->with('parciales',$parciales)->with('claveMateria',$claveMateria);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }  
    
    public function cursoe2($claveMateria,$parcial){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('alumnoscalifparcial')
                ->join('califparcial','califparcial.CalifParcial','=','alumnoscalifparcial.CalifParcial')
                ->join('alumno','alumnoscalifparcial.MatriculaAlumno','=','alumno.MatriculaAlumno')
                ->select(DB::raw("alumno.MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre, calificacion, razon,estado"))
                ->where('noParcial','=',$parcial)
                ->where('claveMateria','=',$claveMateria)
                ->orderBy('alumno.MatriculaAlumno')->get();

                return view('/Reportes/administradorReporteParcialS')->with('alumnos',$consulta);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function semestresPasados(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta = DB::table('semestre')->select('CodigoSemestre','Campus')
                ->where('Activo','=',0)->paginate(10);
                return view('Reportes/administradorReporte')->with('cursos',$consulta);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function semestrePasado($codigoSemestre){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){
                $consulta2 = DB::table('materia')->join('curso', 'materia.clave', '=', 'curso.clave')
                ->select('nombre','Carrera','materia.clave')
                ->where('CodigoSemestre','=', $codigoSemestre)->distinct()->orderBy('Carrera','asc')->paginate(10);
                return view('Reportes/administradorReporteActual')->with('cursos',$consulta2);
            }else{
                return redirect('/home');

            }
        }else{
            return redirect('/home');
        }
    }
}
