<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //uso de la calse DB para poder modificar nuestra base
use Illuminate\Support\Facades\Auth;
use app\Maestro;
use PDF;

class maestroPDFController extends Controller
{
	public function cursosActuales(){
            if(Auth::check()){
                $id = Auth::id();
                $rol = '';
                $CodigoSemestre = 0;
                $matriculaMaestro = 0;
                $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
                foreach($consultaRol as $c){
                    $rol = $c->Rol;
                    $matriculaMaestro =$c->Matricula;
                }
                if($rol=='Maestro'){
                    $semestre = DB::table('semestre')
                    ->select('CodigoSemestre')
                    ->where('Activo','=',1)
                    ->get();

                    foreach($semestre as $c){
                        $CodigoSemestre = $c->CodigoSemestre;
                    }
                    $consulta = DB::table('curso')
                    ->join('materia','materia.clave', '=', 'curso.clave')
                    ->select('ClaveMateria','Grupo','Salon','Nombre','Unidades')
                    ->where('CodigoSemestre','=',$CodigoSemestre)
                    ->where('MatriculaMaestro','=',$matriculaMaestro)->paginate(10);
                    return view('/maestros/materiasReporte')->with('curso', $consulta)->with('claveProfe',$matriculaMaestro);

                }else{
                    return redirect('/home');
                }
            }else{
                return redirect('/home');
            }    
        }
    public function reporte($claveMateria,$matricula){
        $id = Auth::id();
        $rol = '';
        $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
        foreach($consultaRol as $c){
            $rol = $c->Rol;
        }
        if(Auth::check() && $rol=="Maestro"){
            /*$pdf = PDF::loadView('formato');*/
			$consulta = DB::table('curso')
			->join('materia','materia.clave','=','curso.clave')
			->join('semestre','semestre.CodigoSemestre','=','curso.CodigoSemestre')
			->join('maestro','maestro.MatriculaMaestro','=','curso.MatriculaMaestro')
			->select(DB::raw("materia.nombre,materia.carrera,materia.unidades,curso.claveMateria,curso.grupo,curso.salon
				,semestre.campus,CONCAT(maestro.nombre,' ',maestro.apellidoP,' ',maestro.apellidoM)as maestroNombre"))
			->where('curso.claveMateria','=',$claveMateria)
			->get();
			foreach($consulta as $resultados){
				$nombreProf=$resultados->maestroNombre;
				$nombreMateria=$resultados->nombre;
				$carrera=$resultados->carrera;
				$claveGrupo=$resultados->claveMateria;
				$grupo=$resultados->grupo;
				$campus=$resultados->campus;
				$unidades=$resultados->unidades;
			}
			$consultaHorarios = DB::table('curso')
			->join('horariomateria','horariomateria.claveMateria','=','curso.claveMateria')
			->select(DB::raw("CONCAT(horariomateria.horaInicio,' ',horariomateria.horaFin,curso.salon)as hora,horariomateria.dia"))
			->where('curso.claveMateria','=',$claveMateria)
			->get();
			$lunes="";
			$lunesHora="";
			$martes="";
			$martesHora="";
			$miercoles="";
			$miercolesHora="";
			$jueves="";
			$juevesHora="";
			$viernes="";
			$viernesHora="";
			foreach($consultaHorarios as $horario){
				$hora=$horario->hora;
				$dia=$horario->dia;
				if($dia=="lunes"){
					$lunes=$dia;
					$lunesHora=$hora;
				}else if($dia=="martes"){
					$martes=$dia;
					$martesHora=$hora;
				}else if($dia=="miercoles"){
					$miercoles=$dia;
					$miercolesHora=$hora;
				}else if($dia=="jueves"){
					$jueves=$dia;
					$juevesHora=$hora;
				}else if($dia=="viernes"){
					$viernes=$dia;
					$viernesHora=$hora;
				}
			}
			$lunesHora="7:00-8:00 Cd-9";
			$miercolesHora="7:00-8:00 Cd-9";
			$viernesHora="7:00-8:00 Cd-9";
			
			$noAlumnos = DB::table('maestro')
                ->join('curso','curso.MatriculaMaestro','=','maestro.MatriculaMaestro')
                ->join('materiaalumno','materiaalumno.ClaveMateria','=','curso.ClaveMateria')
                ->join('alumno','alumno.MatriculaAlumno','materiaalumno.MatriculaAlumno')
                ->select('alumno.nombre')
                ->where('maestro.MatriculaMaestro','=',$matricula)
                ->where('curso.ClaveMateria','=',$claveMateria)
                ->get();
			$contador=0;
			foreach($noAlumnos as $contadores){
				$contador=$contador+1;
			}
            $calificaciones= DB::table('maestro')
                ->join('curso','curso.MatriculaMaestro','=','maestro.MatriculaMaestro')
                ->join('califParcial','califParcial.ClaveMateria','=','curso.ClaveMateria')
                ->join('alumnoscalifparcial','alumnoscalifparcial.CalifParcial','=','califParcial.CalifParcial')
                ->join('alumno','alumno.MatriculaAlumno','alumnoscalifparcial.MatriculaAlumno')
                ->select('alumno.nombre','califParcial.NoParcial','alumnoscalifparcial.calificacion','alumnoscalifparcial.razon')
                ->where('maestro.MatriculaMaestro','=',$matricula)
                ->where('curso.ClaveMateria','=',$claveMateria)
                ->get();
				$aprobados=0;
				$reprobados=0;
				$reprobadosCalif=0;
				$desertores=0;
				$noParcial=1;
				$aprobadosPocentajes=0;
				$reprobadosPorcentajes=0;
				$porDesercion=0;
				$porBurros=0;
				foreach($calificaciones as $cal){
					if(($cal->NoParcial)==$noParcial){
						if(($cal->razon)=="Aprovado"){
							$aprobados=$aprobados+1;
						}else if(($cal->razon)=="Reprobado"){
							$reprobados=$reprobados+1;
							if(($cal->calificacion)<7){
								$reprobadosCalif=$reprobadosCalif+1;
							}
						}else if(($cal->razon)=="Desercion"){
							$reprobados=$reprobados+1;
							$desertores=$desertores+1;
						}
					}
				}
				$aprobadosPocentajes=(100/$contador)*$aprobados;
				$reprobadosPorcentajes=(100/$contador)*$reprobados;
				$porDesercion=(100/$reprobados)*$reprobadosCalif;
				$porBurros=(100/$reprobados)*$desertores;;
				
			$html='
				<style>

					div #section1{
						width: 100%;
						position:relative;
						line-height: 4px;
						font-size:15px;
					}
					div #section1 #mexicoLogo{
						position:relative;
						float:left;
						width: 7%;
						height: 7%;
						margin-bottom: 0%;
					}
					div #section1 span{
						position:relative;
						top:0;
						text-align: center;
						font-weight: bold;
					}
					div #section1 #itlLogo{
						float:right;
						position:relative;
						width: 7%;
						height: 8%;
					}
					div #section2{
						clear:both;
						width: 100%;
						position: relative;
					}

					div #section2 #cursoInfo{
						font-size:0.8em;
						width: 30%;
						line-height: 4px;
					}
					div #section2 #cursoInfo2{
						width: 100%;
					}
					div #section2 #cursoInfo2 div{
						font-size:0.8em;
						margin-right: 8%;
						float:left;
					}
					div #section2 #horario{
						position:relative;
						transform:translate(140%,-113%);
						font-size: 0.8em;
						width: 40%;
					}
					div #section2 #horario th{
						background: black;
						color:white;
					}
					div #section2 #horario tbody tr td{
						text-align: center;
						width: 20%;
					}

					#cuerpo{
						border: black 2px solid;
						transform:translate(0,-20px);
					}

					#cuerpo .seccion{
						margin-left:5%;
						width: 100%;
						position: relative;
					}
					#cuerpo .seccion h3{
						text-decoration: underline;
						font-size:0.8em;
					}

					#cuerpo .seccion #seccion1{
						font-size: 0.8em;
						text-decoration: none;
						width: 55%;
						position: relative;
					}
					#cuerpo .seccion  #seccion1 .seccion1Campos{
						font-size: 0.8em;
						border:black 1px solid;
						width: 8%;
						height: 1.5em;
					} 

					#cuerpo .seccion  div{
						display:flex;
						height:100px;
					}
					#cuerpo .seccion  div .specialCaracter{
						position:relative;
						font-size: 6em;
						margin-left:55%;
						transform:translate(0,30px);
					}

					#cuerpo .seccion div  #seccion2{
						position:absolute;
						right:2%;
						top:4%;
						font-size: 0.8em;
						width:35%;
					}

					#cuerpo .seccion div  #seccion2 .seccion1Campos2{
						border:black 1px solid;
						font-size: 1.5em;
						width:12%;
					}

					#cuerpo .seccion  #seccion3{
						text-decoration: none;
						width: 90%;
						color:white;
					}
					#cuerpo .seccion #seccion3 tbody tr td{
						border:black 1px solid;
						border-collapse: collapse;
					}
					#cuerpo .seccion .tabla3{
						text-align:center;
						color:white;
						width: 70%;
					}
					#cuerpo .seccion .tabla3 #tabla3{
						border-collapse: collapse;
						border:black 1px solid;
					}
					</style>
<meta http-equiv="Refresh" content="5;{{ url("/maestro/printReporte") }}">
    <div>
            <div id="section1">
                <span style="font-family:"Baskerville Old Face";">
                    <p><img src="C:\xampp\htdocs\minisiiaweb\public\images\logos\mexico.png" id="mexicoLogo"/>INSTITUTO TECNOLOGICO DE LEON  <img src="C:\xampp\htdocs\minisiiaweb\public\images\logos\itl.png" id="itlLogo"/></p>
                    <p>DIVISION DE ESTUDIOS PROFESIONALES</p>
                    <p style="font-size:1em">Este reporte estara disponible hasta el cierra de bajas</p>
                </span>
            </div>
            <div id="section2">
                <div id="cursoInfo">
                    <p>Catedratico: '.$nombreProf.'</p>
                    <p>Asignatura: '.$nombreMateria.'</p>
                    <p>Carrera: '.$carrera.'</p>
                </div>
                <div id="cursoInfo2">
                        <div>Clave Grupo: '.$claveGrupo.'</div>
                        <div>Grupo: '.$grupo.'</div>
                        <div>Campus: '.$campus.'</div>
                </div>
                <table id="horario" cellpadding = "0" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Lunes</th>
                            <th>Martes</th>
                            <th>Miercoles</th>
                            <th>Jueves</th>
                            <th>Viernes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>'.$lunesHora.'</td>
                            <td>'.$martesHora.'</td>
                            <td>'.$miercolesHora.'</td>
                            <td>'.$juevesHora.'</td>
							<td>'.$viernesHora.'</td>
                        </tr>
                        <tr>
                            <td rowspan=5 colspan=5>Impresa el: 04/Junio/2018</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <div id="cuerpo">
            <div class="seccion">
                <h3>1.COBERTURA DEL CURSO</h3>
                <table id="seccion1" cellpadding = "1" cellspacing="1">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="background:black; color:white;">Num</th>
                            <th style="background:black; color:white;">%</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="font-size: 0.8em;">TOTAL DE UNIDADES DEL PROGRAMA</td>
                            <td class="seccion1Campos">'.$unidades.'</td>
                            <td class="seccion1Campos"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 0.8em;">UNIDADES PROGRAMADAS PARA CUBRIRSE A LA FECHA</td>
                            <td class="seccion1Campos"></td>
                            <td class="seccion1Campos"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 0.8em;">UNIDADES CUBIERTAS A LA FECHA</td>
                            <td class="seccion1Campos"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 0.8em;">UNIDADES EVALUADAS, CALIFICADAS Y ENTREGADAS</td>
                            <td class="seccion1Campos"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="seccion">
                <div>
                <h3>2.INDICES DE REPROBACION Y DESERCION</h3>
                    <table id="seccion1" cellpadding = "1" cellspacing="1">
                        <thead>
                            <tr>
                                <th></th>
                                <th style="background:black; color:white;">Num</th>
                                <th style="background:black; color:white;">%</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="font-size: 0.8em;">ALUMNOS INSCRITOS</td>
                                <td class="seccion1Campos">'.$contador.'</td>
                            </tr>
                            <tr>
                                <td style="font-size: 0.8em;">ALUMNOS APROBADOS</td>
                                <td class="seccion1Campos">'.$aprobados.'</td>
                                <td class="seccion1Campos">'.$aprobadosPocentajes.'</td>
                            </tr>
                            <tr>
                                <td style="font-size: 0.8em;">ALUMNOS NO REPROBADOS</td>
                                <td class="seccion1Campos">'.$reprobados.'</td>
                                <td class="seccion1Campos">'.$reprobadosPorcentajes.'</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="specialCaracter">{</div>
                    <table id="seccion2">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th style="background:black; color:white;">Num</th>
                                    <th style="background:black; color:white;">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>POR REPROBACION</td>
                                    <td class="seccion1Campos2">'.$reprobadosCalif.'</td>
                                    <td class="seccion1Campos2">'.$porBurros.'</td>
                                </tr>
                                <tr>
                                    <td>POR DESERCION</td>
                                    <td class="seccion1Campos2">'.$desertores.'</td>
                                    <td class="seccion1Campos2">'.$porDesercion.'</td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
            <div class="seccion">
                <h3>3.DESARROLLO DEL CURSO</h3>
                <table cellpadding = "0" cellspacing="1">
                    <thead>
						<tr>
                        <th style="font-size:0.7em;">EL CURSO SE DESARROLLA </th>
                        <th style="background:black; color:white;padding:0;font-size:0.6em;">
                            REGULAR
                        </th>
                        <th style="width:25px;height:10px; border:black 1px solid;color:white;">,</th>
                        <th style="background:black; color:white;padding:0;font-size:0.6em;">
                            IRREGULAR
                        </th>
                        <th style="width:25px;height:10px; border:black 1px solid;color:white;">,</th>
						</tr>
                    </thead>
                </table>
                <h4 style="font-size:0.8em;">UNIDADES EVALUADAS Y CALIFICACIONES PARCIALES</h4>
                <table class="tabla3" cellpadding = "0" cellspacing="1">
                    <thead>
					<tr>
                        <th style="font-size:0.8em;background:black; color:white;">UNIDAD</th>
                        <th style="font-size:0.8em;background:black; color:white;">FORMA EN LA QUE SE EVALUO</th>
                        <th style="font-size:0.8em;background:black; color:white;">FECHA</th>
						</tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                    </tbody>
                </table>
                <h4 style="font-size:0.8em;">PRACTICAS REALIZADAS</h4>
                <table class="tabla3" cellpadding = "0" cellspacing="1">
                    <thead>
					<tr>
                        <th style="background:black; color:white;font-size:0.8em;">UNIDAD</th>
                        <th style="background:black; color:white;font-size:0.8em;">FORMA EN LA QUE SE EVALUO</th>
                        <th style="background:black; color:white;font-size:0.8em;">FECHA</th>
						</tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="tabla3" style="font-size: 1em;">.</td>
                            <td id="tabla3"style="font-size: 1em;"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3"style="font-size: 1em;">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- <- TERMINA SECCION 3 -->
            <div class="seccion">
                <h3>4.OBSERVACIONES GENERALES</h3>
                <table id="seccion3" cellpadding = "1" cellspacing="0">
                    <thead>
						<tr>
							<th>.</th>
						</tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>.</td>
                        </tr>
                        <tr>
                            <td>.</td>
                        </tr>
                        <tr>
                            <td>.</td>
                        </tr>
                        <tr>
                            <td>.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br><br>
            <hr width=30%>
            <p align="center">Firma del catedratico</p>
            <!-- Termina -->
        </div>
    </div>
			';
            $pdf = PDF::loadhtml($html);
            return $pdf->stream('invoice.pdf');
            
        }else{
            return redirect('/home');
        }
    }
}