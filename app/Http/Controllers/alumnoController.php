<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class alumnoController extends Controller
{
    public function store(Request $request){
        $MatriculaAlumno = $request->input('matricula');
        $apellidoP = $request->input('apellidoP');
        $apellidoM = $request->input('apellidoM');
        $nombre = $request->input('nombre');
        $carrera = $request->input('carrera');
        $telefono = $request->input('telefono');
        $email = $request->input('email');
        $añoIngreso = $request->input('añoIngreso');
        $mesIngreso = $request->input('mesIngreso');
        if($request->input('genero'=="Masculino")){
            $genero = true;
        }else{
            $genero = false;
        }
        $usuario = Auth::id();

        $insertado = DB::insert('insert into alumno (MatriculaAlumno, nombre, apellidoP, apellidoM,
            Genero, Carrera, Correo, telefono,añoIngreso,mesIngreso ,Activo, fechaCreacion, id)
            values(?,?,?,?,?,?,?,?,?,?,true,NOW(),?);',
            [$MatriculaAlumno,$nombre,$apellidoP,$apellidoM,$genero,$carrera,$email,$telefono,$añoIngreso,$mesIngreso,$usuario]);

            if($insertado){
                $pass = bcrypt($MatriculaAlumno);
                
                $usuario = DB::table('users')->insertGetId(['name' => 'alumno', 'email' => $email,'username' => $MatriculaAlumno,'password' => $pass]);
                $rol = DB::table('roles')->insert(['id' => $usuario, 'Rol' => 'Alumno','Matricula' => $MatriculaAlumno]);
                $resultado = "Alumno con matricula: ".$MatriculaAlumno." a sido agregado";
                return view('/registro/alumno')->with('resultado', $resultado);
            }else{
                $resultado = "Alumno no agregado";
                return view('/registro/alumno')->with('resultado', $resultado);
            }
    
    }

    public function datos(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){   
                $resultado=null;
                 $consulta = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera,correo,telefono"))
                 ->where('Activo', '=', 1)->paginate(10);
                 return view('/eliminacion/eliminarAlumno')->with('alumnos',$consulta)->with('resultado',$resultado);
            }else{
                return redirect('/home');
            }
        }else{
             return redirect('/home');
    }
    }

    public function editar( $matricula){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){  
                $resultado=null;
                $consulta = DB::select('SELECT MatriculaAlumno, nombre,apellidoP,apellidoM,genero,Carrera,correo,telefono,añoIngreso,mesIngreso from alumno where Activo = true and  MatriculaAlumno = ?;',[$matricula]);
                return view('/modificacion/modificarAlumno')->with('datos',$consulta)->with('resultado',$resultado);
        }else{
            return redirect('/home');
        }
        }else{
        return redirect('/home');
}
    }

    public function modificar(Request $request){
        $MatriculaAlumno = $request->input('matricula');
        $MatriculaAlumnoAnterior = $request->input('matriculaAnterior');
        $apellidoP = $request->input('apellidoP');
        $apellidoM = $request->input('apellidoM');
        $nombre = $request->input('nombre');
        $carrera = $request->input('carrera');
        $telefono = $request->input('telefono');
        $email = $request->input('email');
        $añoIngreso = $request->input('añoIngreso');
        $mesIngreso = $request->input('mesIngreso');
        if($request->input('genero'=="Masculino")){
            $genero = true;
        }else{
            $genero = false;
        }
        
        $consulta = DB::update("UPDATE alumno set MatriculaAlumno = ?, nombre= ?, apellidoP = ?, apellidoM = ?, genero = ?, carrera = ?, correo = ?, telefono = ?, añoIngreso = ?, mesIngreso = ? where MatriculaAlumno = ?;",
        [$MatriculaAlumno,$nombre,$apellidoP,$apellidoM,$genero,$carrera,$email,$telefono,$añoIngreso,$mesIngreso,$MatriculaAlumnoAnterior]);

        if($consulta){
            $resultado = "Alumno con matricula: ".$MatriculaAlumnoAnterior." a sido actualizado";
            $consulta2 = DB::select('SELECT MatriculaAlumno, nombre,apellidoP,apellidoM,genero,Carrera,correo,telefono,añoIngreso,mesIngreso from alumno where Activo = true and  MatriculaAlumno = ?;',[$MatriculaAlumno]);
            return view('/modificacion/modificarAlumno')->with('datos',$consulta2)->with('resultado',$resultado);
        }else{
            $resultado = "El alumno no ha podido ser eliminado";
            $consulta2 = DB::select('SELECT MatriculaAlumno, nombre,apellidoP,apellidoM,genero,Carrera,correo,telefono,añoIngreso,mesIngreso from alumno where Activo = true and  MatriculaAlumno = ?;',[$MatriculaAlumnoAnterior]);
            return view('/modificacion/modificarAlumno')->with('datos',$consulta2)->with('resultado',$resultado);
        }

    }

    public function eliminar($matricula){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){  
                $consulta = DB::update('UPDATE alumno set Activo = 0 where MatriculaAlumno = ?;',[$matricula]);
                if($consulta){
                    $resultado = "Alumno con matricula: ".$matricula." a sido eliminado";
                    $consulta2 = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera,correo,telefono"))
                    ->where('Activo', '=', 1)->paginate(10);
                    return view('/eliminacion/eliminarAlumno')->with('alumnos',$consulta2)->with('resultado',$resultado);
                }else{
                    $resultado = "El alumno no ha podido ser eliminado";
                    $consulta2 = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera,correo,telefono"))
                    ->where('Activo', '=', 1)->paginate(10);
                    return view('/eliminacion/eliminarAlumno')->with('alumnos',$consulta2)->with('resultado',$resultado);
                }
            }else{
                return redirect('/home');
            }
            }else{
            return redirect('/home');
}
    }

    public function buscar(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){ 
                $consulta = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera,correo,telefono,Genero,añoIngreso,mesIngreso"))
                ->where('Activo', '=', 1)->paginate(10);
                return view('/Busquedas/busquedaAlumno')->with('alumnos',$consulta);
        }else{
            return redirect('/home');
        }
        }else{
        return redirect('/home');
}
    }
    public function buscarParcial(Request $request){
        $MatriculaAlumno = $request->input('matricula');
        $consulta = DB::table('alumno')->select(DB::raw("MatriculaAlumno, CONCAT(nombre,' ',apellidoP,' ',apellidoM) as nombre,Carrera,correo,telefono,Genero,añoIngreso,mesIngreso"))
        ->where('MatriculaAlumno' ,'LIKE','%'.$MatriculaAlumno.'%')
        ->where('Activo', '=', 1)->paginate(10);
        return view('/Busquedas/busquedaAlumno')->with('alumnos',$consulta);
    }

    /* Obtiene las calificaciones de un curso */
    public function verCalificaciones($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            $alumno=DB::table('roles')
                ->select('Matricula')
                ->where('id','=',$id)
                ->get();
                foreach($alumno as $c){
                    $matriculaAlumno = $c->Matricula;
                }
            if($rol=='Alumno'){ 
                $consulta = DB::table('users')
                ->join('alumno','users.id','=','alumno.id')
                ->join('alumnoscalifparcial','alumnoscalifparcial.MatriculaAlumno','=','alumno.MatriculaAlumno')
                ->join('califParcial','califParcial.califParcial','=','alumnoscalifparcial.califParcial')
                ->join('curso','curso.claveMateria','=','califParcial.claveMateria')
                ->join('materia','curso.clave','=','materia.clave')
                ->select('alumnoscalifparcial.calificacion','materia.nombre','califParcial.NoParcial')
                ->where('alumno.MatriculaAlumno','=',$matriculaAlumno)
                ->where('curso.ClaveMateria','=',$claveMateria)
                ->get();
                return view('/alumnos/calificaciones')->with('calificaciones',$consulta);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    /* Busca los cursos asignados al alumno, funcion utilizada para ver la grafica por materia*/
    public function materias(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Alumno'){
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();
                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $alumno=DB::table('roles')
                ->select('Matricula')
                ->where('id','=',$id)
                ->get();
                foreach($alumno as $c){
                    $matriculaAlumno = $c->Matricula;
                }
                $consulta = DB::table('users')
                ->join('alumno','users.id','=','alumno.id')
                ->join('materiaalumno','alumno.MatriculaAlumno','=','materiaalumno.MatriculaAlumno')
                ->join('curso','materiaalumno.ClaveMateria','=','curso.ClaveMateria')
                ->join('materia','curso.clave','=','materia.clave')
                ->select('materia.nombre','curso.ClaveMateria','materia.Unidades','materia.clave')
                ->where('alumno.MatriculaAlumno','=',$matriculaAlumno)
                ->where('curso.CodigoSemestre','=',$CodigoSemestre)
                ->get();
                return view('/alumnos/materias')->with('materias',$consulta);
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function cursosActuales(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $matricula = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matricula = $c->Matricula;
            }
            if($rol=='Alumno'){  
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();
                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $resultado=null;
                $consulta = DB::table('materiaalumno')
                ->join('curso','curso.clavemateria','=','materiaalumno.clavemateria')
                ->join('materia','materia.clave','=','curso.clave')
                ->select('curso.claveMateria','Grupo','Salon','nombre')
                ->where('CodigoSemestre','=',$CodigoSemestre)
                ->where('MatriculaAlumno','=',$matricula)->paginate(20);
                return view('/interfaz/alumnoCursos')->with('materia',$consulta);
        }else{
            return redirect('/home');
        }
        }else{
            return redirect('/home');
        }
    }

    public function mostrarFinales(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $matricula = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matricula = $c->Matricula;
            }
            if($rol=='Alumno'){  
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();
                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('califFInal')
                ->join('curso','curso.clavemateria','=','califFInal.clavemateria')
                ->join('materia','materia.clave','=','curso.clave')
                ->select('curso.claveMateria','Grupo','Salon','nombre','Calificacion','Estado','Razon')
                ->where('CodigoSemestre','=',$CodigoSemestre)
                ->where('califFinal.MatriculaAlumno','=',$matricula)->paginate(20);
                return view('/interfaz/alumnoCursosF')->with('materia',$consulta);
        }else{
            return redirect('/home');
        }
        }else{
            return redirect('/home');
        }
    }

    public function finalCurso($claveMateria){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $matricula = 0;
            $consultaRol = DB::table('roles')->select('Rol','Matricula')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
                $matricula = $c->Matricula;
            }
            if($rol=='Alumno'){  
                $semestre = DB::table('semestre')
                ->select('CodigoSemestre')
                ->where('Activo','=',1)
                ->get();
                foreach($semestre as $c){
                    $CodigoSemestre = $c->CodigoSemestre;
                }
                $consulta = DB::table('califfinal')
                ->select('claveMateria','Calificacion','Estado','razon')
                ->where('claveMateria','=',$claveMateria)
                ->where('MatriculaAlumno','=',$matricula)->paginate(20);
                return view('/interfaz/alumnoCursosF')->with('materia',$consulta);
        }else{
            return redirect('/home');
        }
        }else{
            return redirect('/home');
        }
    }


}