<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class modificarCalifController extends Controller
{
    public function modParcial(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){  
            return view('registro/clavesParcial')->with('resultado','');
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function storeParcial(Request $request){
        $claveMateria = $request->input('claveMateria');
        $noParcial = $request->input('noParcial');
        $matriculaMaestro = $request->input('matriculaMaestro');
        $matriculaAlumno = $request->input('matriculaAlumno');
        $descripcion = $request->input('descripcion');
        $con1=0;
        $numero  = '';
        $extra = 'pl';
        $id = Auth::id();

        $comMaestro = DB::table('maestro')->select('MatriculaMaestro')
        ->where('MatriculaMaestro','=',$matriculaMaestro)->get();
        foreach($comMaestro as $c){
            $con1 = 1;
        }
        if($con1==1){
            $comCurso = DB::table('curso')->select('ClaveMateria')
            ->where('ClaveMateria','=',$claveMateria)
            ->where('MatriculaMaestro','=',$matriculaMaestro)->get();
            foreach($comCurso as $c){
                $con1 = 2;
            }
            if($con1==2){
                $conAlumno = DB::table('materiaalumno')->select('MatriculaAlumno')
                ->where('MatriculaAlumno','=',$matriculaAlumno)
                ->where('ClaveMateria','=',$claveMateria)->get();
                foreach($conAlumno as $c){
                    $con1 = 3;
                }
                if($con1==3){
                    $com = 1;
                    do{
                        $con = 0;
                        for($i=0;$i<11;$i++){
                            $con1 = rand(1,9);
                            $numero = $numero.$con1;
                        }
                        $numero = $numero.$extra;
                        $consulta = DB::table('clavesModParcial')->select('ClaveModParcial')
                        ->where('ClaveModParcial','=',$numero)->get();
                        foreach($consulta as $c){
                            $con=1;
                        }
                        if($con==1){
                            $numero  = '';
                        }
                        if($con==0){
                            break;
                        }
                    }while(0);
                    $califParcial = 0;
                    
                    $bCalifParcial = DB::table('califparcial')->select('CalifParcial')
                    ->where('ClaveMateria','=',$claveMateria)
                    ->where('NoParcial','=',$noParcial)->get();

                    foreach($bCalifParcial as $c){
                        $califParcial = $c->CalifParcial;
                    }


                    $insertar  = DB::table('clavesmodparcial')
                    ->insert(['ClaveModParcial' => $numero,'CalifParcial' => $califParcial,
                    'MatriculaAlumno' => $matriculaAlumno, 'MatriculaMaestro' => $matriculaMaestro,'Descripcion' => $descripcion,
                    'Activo' => 1,'id' => $id]);
                    if($insertar){
                        $mensaje = 'Clave = '.$numero;
                        return view('/registro/clavesParcial')->with('resultado',$mensaje);
                    }else{
                        return view('/registro/clavesParcial')->with('resultado','No se pudo agregar');
                    }

                }else{
                    return view('/registro/clavesParcial')->with('resultado','El alumno no pertenece al curso especificado');
                }
            }else{
                return view('/registro/clavesParcial')->with('resultado','Curso no encontrado, favor de revisar la ClaveMateria y la Matricula del maestro');
            }
        }else{
            return view('/registro/clavesParcial')->with('resultado','Matricula del maestro incorrecta');
        }

        $comMateria = DB::table('curso')->select('ClaveMateria')
        ->where('ClaveMateria','=',$claveMateria)->get();
    }

    public function reactivarParcial(Request $request){
        $clave = $request->input('clave');
        $claveMateria = $request->input('claveMateria');
        $parcial = $request->input('parcial');
        $id = Auth::id();
        $matriculaMaestro = 0;
        $matriculaMaestro2 = 0;
        $matriculaAlumno = 0;
        $califParcial = 0;
        $con = 0;

        $consultaRol = DB::table('roles')->select('Matricula')->where('id','=',$id)->get();
        foreach($consultaRol as $c){
            $matriculaMaestro =$c->Matricula;
        }

        $datos = DB::table('clavesmodparcial')
        ->select('CalifParcial','MatriculaAlumno','MatriculaMaestro')
        ->where('ClaveModParcial','=',$clave)
        ->where('Activo', '=', 1)
        ->get();
        foreach($datos as $c){
            $con = 1;
            $matriculaMaestro2 =$c->MatriculaMaestro;
            $matriculaAlumno = $c->MatriculaAlumno;
            $califParcial = $c->CalifParcial;
        }
        if($con==1){
            if($matriculaMaestro == $matriculaMaestro2){
                $actualizar = DB::table('alumnoscalifparcial')
                ->where('CalifParcial','=',$califParcial)
                ->where('MatriculaAlumno','=',$matriculaAlumno)
                ->update(["Acentada" => 0]);
                $actualizar2 = DB::table('clavesmodparcial')
                ->where('ClaveModParcial','=',$clave)
                ->update(['Activo' => 0]);
                return redirect()->action('maestroCursosController@cursoCalificar', [$claveMateria,$parcial]);
            }else{
                return redirect()->action('maestroCursosController@cursoCalificar', [$claveMateria,$parcial]);
                
            }
        }else{
            return redirect()->action('maestroCursosController@cursoCalificar', [$claveMateria,$parcial]);
           
        }
        
     }

     public function modFinal(){
        if(Auth::check()){
            $id = Auth::id();
            $rol = '';
            $consultaRol = DB::table('roles')->select('Rol')->where('id','=',$id)->get();
            foreach($consultaRol as $c){
                $rol = $c->Rol;
            }
            if($rol=='Administrador'){  
            return view('registro/clavesFinal')->with('resultado','');
            }else{
                return redirect('/home');
            }
        }else{
            return redirect('/home');
        }
    }

    public function storeFinal(Request $request){
        $claveMateria = $request->input('claveMateria');
        $matriculaMaestro = $request->input('matriculaMaestro');
        $matriculaAlumno = $request->input('matriculaAlumno');
        $descripcion = $request->input('descripcion');
        $con1=0;
        $numero  = '';
        $extra = 'pf';
        $id = Auth::id();

        $comMaestro = DB::table('maestro')->select('MatriculaMaestro')
        ->where('MatriculaMaestro','=',$matriculaMaestro)->get();
        foreach($comMaestro as $c){
            $con1 = 1;
        }
        if($con1==1){
            $comCurso = DB::table('curso')->select('ClaveMateria')
            ->where('ClaveMateria','=',$claveMateria)
            ->where('MatriculaMaestro','=',$matriculaMaestro)->get();
            foreach($comCurso as $c){
                $con1 = 2;
            }
            if($con1==2){
                $conAlumno = DB::table('materiaalumno')->select('MatriculaAlumno')
                ->where('MatriculaAlumno','=',$matriculaAlumno)
                ->where('ClaveMateria','=',$claveMateria)->get();
                foreach($conAlumno as $c){
                    $con1 = 3;
                }
                if($con1==3){
                    $com = 1;
                    do{
                        $con = 0;
                        for($i=0;$i<11;$i++){
                            $con1 = rand(1,9);
                            $numero = $numero.$con1;
                        }
                        $numero = $numero.$extra;
                        $consulta = DB::table('clavesModFinal')->select('ClaveModFinal')
                        ->where('ClaveModFinal','=',$numero)->get();
                        foreach($consulta as $c){
                            $con=1;
                        }
                        if($con==1){
                            $numero  = '';
                        }
                        if($con==0){
                            break;
                        }
                    }while(0);
                    $califFinal = 0;
                    
                    $bCalifFinal = DB::table('califFinal')->select('CalifFinal')
                    ->where('ClaveMateria','=',$claveMateria)
                    ->where('MatriculaAlumno','=',$matriculaAlumno)->get();

                    foreach($bCalifFinal as $c){
                        $califFinal = $c->CalifFinal;
                    }


                    $insertar  = DB::table('clavesmodfinal')
                    ->insert(['ClaveModFinal' => $numero,'ClaveMateria' => $claveMateria,
                    'MatriculaAlumno' => $matriculaAlumno, 'MatriculaMaestro' => $matriculaMaestro,'Descripcion' => $descripcion,
                    'Activo' => 1,'id' => $id]);
                    if($insertar){
                        $mensaje = 'Clave = '.$numero;
                        return view('/registro/clavesFinal')->with('resultado',$mensaje);
                    }else{
                        return view('/registro/clavesFinal')->with('resultado','No se pudo agregar');
                    }

                }else{
                    return view('/registro/clavesFinal')->with('resultado','El alumno no pertenece al curso especificado');
                }
            }else{
                return view('/registro/clavesFinal')->with('resultado','Curso no encontrado, favor de revisar la ClaveMateria y la Matricula del maestro');
            }
        }else{
            return view('/registro/clavesFinal')->with('resultado','Matricula del maestro incorrecta');
        }

        $comMateria = DB::table('curso')->select('ClaveMateria')
        ->where('ClaveMateria','=',$claveMateria)->get();
    }

    public function reactivarFinal(Request $request){
        $clave = $request->input('clave');
        $claveMateria = $request->input('claveMateria');
        $id = Auth::id();
        $matriculaMaestro = 0;
        $matriculaMaestro2 = 0;
        $matriculaAlumno = 0;
        $claveMateria2 = 0;
        $con = 0;

        $consultaRol = DB::table('roles')->select('Matricula')->where('id','=',$id)->get();
        foreach($consultaRol as $c){
            $matriculaMaestro =$c->Matricula;
        }

        $datos = DB::table('clavesmodFinal')
        ->select('ClaveMateria','MatriculaAlumno','MatriculaMaestro')
        ->where('ClaveModFinal','=',$clave)
        ->where('Activo', '=', 1)
        ->get();
        foreach($datos as $c){
            $con = 1;
            $matriculaMaestro2 =$c->MatriculaMaestro;
            $matriculaAlumno = $c->MatriculaAlumno;
            $claveMateria2 = $c->ClaveMateria;
        }
        if($con==1){
            if($matriculaMaestro == $matriculaMaestro2){
                $actualizar = DB::table('califfinal')
                ->where('ClaveMateria','=',$claveMateria2)
                ->where('MatriculaAlumno','=',$matriculaAlumno)
                ->update(["Acentada" => 0]);
                $actualizar2 = DB::table('clavesmodFinal')
                ->where('ClaveModFinal','=',$clave)
                ->update(['Activo' => 0]);
                return redirect()->action('caliFinalesController@cursoCalificar', [$claveMateria]);
            }else{
                return redirect()->action('caliFinalesController@cursoCalificar', [$claveMateria]);
                
            }
        }else{
            return redirect()->action('caliFinalesController@cursoCalificar', [$claveMateria]);
           
        }
        
     }

}
