@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Usuarios")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
		<th>Id</th>
		<th>Correo</th>
        <th>Contraseña</th>
        <th>Rol </th>
        <th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($usuarios as $usuario)
		<tr>
		<td>{{$usuario->id}}</td>
		<td>{{$usuario->email}}</td>
		<td>{{$usuario->password}}</td>
		<td>{{$usuario->Rol}}</td>
		<td>
			<a href="{{ URL('/usuario/editar',$usuario->id) }}">Editar</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $usuarios->links() }}
@include('footer')
@endsection('content')