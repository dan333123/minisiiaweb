@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Maestro")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
    <link rel = "stylesheet" href = "{{ asset('css/FormularioBusqueda.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')


<form id = "Busqueda" role="form" method="post" action="{{ url('/alumno/buscarParcial') }}">
{!! csrf_field() !!}
<legend>Busqueda</legend>
<p>
    <label for ="matricula">Matricula Alumno:</label> 
	<input type="text" name = "matricula" id = "matricula" size = "30" maxlength = "20" placeholder="Matricula del alumno" autofocus required><br/>
    <div class = "boton">
        <input type="submit" value="Enviar" for "matricula" ></code>	
    </div>
</p>
</form>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula</th>
        <th>Nombre</th>
        <th>Genero</th>
        <th>Carrera </th>
        <th>Correo </th>
		<th>Telefono </th>
        <th>Año ingreso</th>
        <th>Mes ingreso</th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($alumnos as $alumno)
		<tr>
		<td>{{$alumno->MatriculaAlumno}}</td>
		<td>{{$alumno->nombre}}</td>
        @if ($alumno->Genero==0)
        <td>Hombre</td>>
        @else
        <td>Mujer</td>
        @endif
		<td>{{$alumno->Carrera}}</td>
		<td>{{$alumno->correo}}</td>
		<td>{{$alumno->telefono}}</td>
        <td>{{$alumno->añoIngreso}}</td>
        <td>{{$alumno->mesIngreso}}</td>
		<td>
			<a href="{{ URL('/alumno/editar',$alumno->MatriculaAlumno) }}">Editar</a>
			<a href="{{ URL('/alumno/eliminar',$alumno->MatriculaAlumno) }}">Eliminar</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $alumnos->links() }}
@include('footer')
@endsection('content')