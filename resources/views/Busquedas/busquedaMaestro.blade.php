@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Maestro")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
    <link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
    <link rel = "stylesheet" href = "{{ asset('css/FormularioBusqueda.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
<form id = "Busqueda" role="form" method="post" action="{{ url('/maestro/buscarParcial') }}">
{!! csrf_field() !!}
<legend>Busqueda</legend>
<p>
    <label for ="matricula">Matricula Maestro:</label> 
	<input type="text" name = "matricula" id = "matricula" size = "30" maxlength = "20" placeholder="Matricula del Maestro" autofocus required><br/>
    <div class = "boton">
        <input type="submit" value="Enviar" for "matricula" ></code>	
    </div>
</p>
</form>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula</th>
        <th>Nombre</th>
        <th>Genero</th>
        <th>Departamento </th>
        <th>Correo </th>
		<th>Telefono </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
    @foreach ($maestros as $maestro)
		<tr>
		<td>{{$maestro->matriculaMaestro}}</td>
        <td>{{$maestro->nombre}}</td>
        @if ($maestro->genero==0)
        <td>Hombre</td>>
        @else
        <td>Mujer</td>
        @endif
		<td>{{$maestro->Departamento}}</td>
		<td>{{$maestro->correo}}</td>
		<td>{{$maestro->telefono}}</td>
		<td>
			<a href="{{ URL('/maestro/editar',$maestro->matriculaMaestro) }}">Editar</a>
			<a href="{{ URL('/maestro/eliminar',$maestro->matriculaMaestro) }}">Eliminar</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $maestros->links() }}
@include('footer')
@endsection('content')