<style>

div #section1{
    width: 100%;
    position:relative;
    line-height: 4px;
    font-size:15px;
}
div #section1 #mexicoLogo{
    position:relative;
    float:left;
    width: 7%;
    height: 7%;
    margin-bottom: 0%;
}
div #section1 span{
    position:relative;
    top:0;
    text-align: center;
    font-weight: bold;
}
div #section1 #itlLogo{
    float:right;
    position:relative;
    width: 7%;
    height: 8%;
}
div #section2{
    clear:both;
    width: 100%;
    position: relative;
}

div #section2 #cursoInfo{
    font-size:0.8em;
    width: 30%;
    line-height: 4px;
}
div #section2 #cursoInfo2{
    width: 100%;
}
div #section2 #cursoInfo2 div{
    font-size:0.8em;
    margin-right: 8%;
    float:left;
}
div #section2 #horario{
    position:relative;
    transform:translate(140%,-113%);
    font-size: 0.8em;
    width: 40%;
}
div #section2 #horario th{
    background: black;
    color:white;
}
div #section2 #horario tbody tr td{
    text-align: center;
    width: 20%;
}

#cuerpo{
    border: black 2px solid;
    transform:translate(0,-20px);
}

#cuerpo .seccion{
    margin-left:5%;
    width: 100%;
    position: relative;
}
#cuerpo .seccion h3{
    text-decoration: underline;
    font-size:0.8em;
}

#cuerpo .seccion #seccion1{
    font-size: 0.8em;
    text-decoration: none;
    width: 55%;
    position: relative;
}
#cuerpo .seccion  #seccion1 .seccion1Campos{
    font-size: 0.8em;
    border:black 1px solid;
    width: 8%;
    height: 1.5em;
} 

#cuerpo .seccion  div{
    display:flex;
    height:100px;
}
#cuerpo .seccion  div .specialCaracter{
    position:relative;
    font-size: 6em;
    margin-left:55%;
}

#cuerpo .seccion div  #seccion2{
    position:absolute;
    right:2%;
    top:4%;
    font-size: 0.8em;
    width:35%;
}

#cuerpo .seccion div  #seccion2 .seccion1Campos2{
    border:black 1px solid;
    font-size: 1.5em;
    width:12%;
}

/* <---- */
#cuerpo .seccion  #seccion3{
    text-decoration: none;
    width: 90%;
    color:white;
}
#cuerpo .seccion #seccion3 tbody tr td{
    border:black 1px solid;
    border-collapse: collapse;
}
#cuerpo .seccion .tabla3{
    text-align:center;
    color:white;
    width: 70%;
}
#cuerpo .seccion .tabla3 #tabla3{
    border-collapse: collapse;
    border:black 1px solid;
}
</style>
<script>
    var mensaje;
</script>
@foreach($consulta as $mensaje)
    <script>
    mensaje="{{$mensaje->Rol}}";  
</script>
@endforeach
<meta http-equiv="Refresh" content="5;{{ url('/maestro/printReporte') }}">
    <div>
            <div id="section1">
                <span style="font-family:'Baskerville Old Face';">
                    <p><img src="C:\xampp\htdocs\minisiiaweb\public\images\logos\mexico.png" id="mexicoLogo"/>INSTITUTO TECNOLOGICO DE LEON  <img src="C:\xampp\htdocs\minisiiaweb\public\images\logos\itl.png" id="itlLogo"/></p>
                    <p>DIVISION DE ESTUDIOS PROFESIONALES</p>
                    <p style="font-size:1em">Este reporte estara disponible hasta el cierra de bajas</p>
                </span>
            </div>
            <div id="section2">
                <div id="cursoInfo">
                    <p>Catedratico:</p>
                    <p>Asignatura:</p>
                    <p>Carrera:</p>
                </div>
                <div id="cursoInfo2">
                        <div>Clave Grupo:</div>
                        <div>Grupo:</div>
                        <div>Campus:</div>
                </div>
                <table id="horario" cellpadding = "0" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Lunes</th>
                            <th>Martes</th>
                            <th>Miercoles</th>
                            <th>Jueves</th>
                            <th>Viernes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>7:00-8:40 C-D8</td>
                            <td ></td>
                            <td></td>
                            <td ></td>
                        </tr>
                        <tr>
                            <td rowspan=5 colspan=5>Impresa el: 10/02/2018</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <div id="cuerpo">
            <div class="seccion">
                <h3>1.COBERTURA DEL CURSO</h3>
                <table id="seccion1" cellpadding = "1" cellspacing="1">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="background:black; color:white;">Num</th>
                            <th style="background:black; color:white;">%</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="font-size: 0.8em;">TOTAL DE UNIDADES DEL PROGRAMA</td>
                            <td class="seccion1Campos"></td>
                            <td class="seccion1Campos"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 0.8em;">UNIDADES PROGRAMADAS PARA CUBRIRSE A LA FECHA</td>
                            <td class="seccion1Campos"></td>
                            <td class="seccion1Campos"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 0.8em;">UNIDADES CUBIERTAS A LA FECHA</td>
                            <td class="seccion1Campos"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 0.8em;">UNIDADES EVALUADAS, CALIFICADAS Y ENTREGADAS</td>
                            <td class="seccion1Campos"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="seccion">
                <div>
                <h3>2.INDICES DE REPROBACION Y DESERCION</h3>
                    <table id="seccion1" cellpadding = "1" cellspacing="1">
                        <thead>
                            <tr>
                                <th></th>
                                <th style="background:black; color:white;">Num</th>
                                <th style="background:black; color:white;">%</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="font-size: 0.8em;">ALUMNOS INSCRITOS</td>
                                <td class="seccion1Campos"></td>
                            </tr>
                            <tr>
                                <td style="font-size: 0.8em;">ALUMNOS APROBADOS</td>
                                <td class="seccion1Campos"></td>
                                <td class="seccion1Campos"></td>
                            </tr>
                            <tr>
                                <td style="font-size: 0.8em;">ALUMNOS NO REPROBADOS</td>
                                <td class="seccion1Campos"></td>
                                <td class="seccion1Campos"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="specialCaracter">{</div>
                    <table id="seccion2">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th style="background:black; color:white;">Num</th>
                                    <th style="background:black; color:white;">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>POR REPROBACION</td>
                                    <td class="seccion1Campos2"></td>
                                    <td class="seccion1Campos2"></td>
                                </tr>
                                <tr>
                                    <td>POR DESERCION</td>
                                    <td class="seccion1Campos2"></td>
                                    <td class="seccion1Campos2"></td>
                                </tr>
                            </tbody>
                    </table>
                </div>
            </div>
            <div class="seccion">
                <h3>3.DESARROLLO DEL CURSO</h3>
                <table cellpadding = "0" cellspacing="1">
                    <thead>
						<tr>
                        <th style="font-size:0.7em;">EL CURSO SE DESARROLLA </th>
                        <th style="background:black; color:white;padding:0;font-size:0.6em;">
                            REGULAR
                        </th>
                        <th style="width:25px;height:10px; border:black 1px solid;color:white;">,</th>
                        <th style="background:black; color:white;padding:0;font-size:0.6em;">
                            IRREGULAR
                        </th>
                        <th style="width:25px;height:10px; border:black 1px solid;color:white;">,</th>
						</tr>
                    </thead>
                </table>
                <h4 style="font-size:0.8em;">UNIDADES EVALUADAS Y CALIFICACIONES PARCIALES</h4>
                <table class="tabla3" cellpadding = "0" cellspacing="1">
                    <thead>
					<tr>
                        <th style="font-size:0.8em;background:black; color:white;">UNIDAD</th>
                        <th style="font-size:0.8em;background:black; color:white;">FORMA EN LA QUE SE EVALUO</th>
                        <th style="font-size:0.8em;background:black; color:white;">FECHA</th>
						</tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                    </tbody>
                </table>
                <h4 style="font-size:0.8em;">PRACTICAS REALIZADAS</h4>
                <table class="tabla3" cellpadding = "0" cellspacing="1">
                    <thead>
					<tr>
                        <th style="background:black; color:white;font-size:0.8em;">UNIDAD</th>
                        <th style="background:black; color:white;font-size:0.8em;">FORMA EN LA QUE SE EVALUO</th>
                        <th style="background:black; color:white;font-size:0.8em;">FECHA</th>
						</tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="tabla3" style="font-size: 1em;">.</td>
                            <td id="tabla3"style="font-size: 1em;"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3"style="font-size: 1em;">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                        <tr>
                            <td id="tabla3">.</td>
                            <td id="tabla3"></td>
                            <td id="tabla3"></td>
                        </tr>
                    </tbody>
                </table>
            </div> <!-- <- TERMINA SECCION 3 -->
            <div class="seccion">
                <h3>4.OBSERVACIONES GENERALES</h3>
                <table id="seccion3" cellpadding = "1" cellspacing="0">
                    <thead>
						<tr>
							<th>.</th>
						</tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>.</td>
                        </tr>
                        <tr>
                            <td>.</td>
                        </tr>
                        <tr>
                            <td>.</td>
                        </tr>
                        <tr>
                            <td>.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br><br>
            <hr width=30%>
            <p align="center">Firma del catedratico</p>
            <!-- Termina -->
        </div>
    </div>