@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Reporte-Semestre-Actual")

@section('estilos_adicionales')
<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave Curso</th>
        <th>Maestro</th>
        <th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($cursos as $curso)
		<tr>
        <td>{{$curso->ClaveMateria}}</td>
        <td>{{$curso->nombreCompleto}}</td>    
		<td>
			<a href="{{ URL('/Administrador/Reporte/Actual/CursoA',$curso->ClaveMateria) }}">Ver</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $cursos->links() }}
@include('footer')
@endsection('content')