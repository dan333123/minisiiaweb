@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Reporte-Semestre")

@section('estilos_adicionales')
<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Codigo Semestre</th>
        <th>Plantel</th>
        <th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($cursos as $curso)
		<tr>
        <td>{{$curso->CodigoSemestre}}</td>
        <td>{{$curso->Campus}}</td>    
		<td>
			<a href="{{ URL('/Administrador/Reporte',$curso->CodigoSemestre) }}">Ver</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $cursos->links() }}
@include('footer')
@endsection('content')