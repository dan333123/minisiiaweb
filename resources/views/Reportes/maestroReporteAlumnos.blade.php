@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Cursos-Calificar")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
    
@endsection('estilos_adicionales')

@section('content')
{{$a = 1}}
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula Alumno</th>
        <th>Nombre</th>
        <th>Semestre Alumno </th>
		<th>Curso </th>
	</tr>
	</thead>
	<tbody>
	@foreach ($alumnos as $alumno)
		<tr>
		<td>{{$alumno->MatriculaAlumno}}</td>
		<td>{{$alumno->nombre}}</td>
		<td>{{$alumno->SemestreAlumno}}</td>
		<td>{{$alumno->Curso}}</td>
		</tr>
	@endforeach
</table> 
{{ $alumnos->links() }}    
	@include('footer')
@endsection('content')