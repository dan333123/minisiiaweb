@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Asignacion-Curso")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula ALumno</th>
        <th>Nombre</th>
        <th>Semestre Alumno</th>
        <th>Curso </th>
	</tr>
	</thead>
	<tbody>
	@foreach ($cursos as $curso)
		<tr>
		<td>{{$curso->MatriculaALumno}}</td>
		<td>{{$curso->nombreC}}</td>
		<td>{{$curso->SemestreAlumno}}</td>
		<td>{{$curso->Curso}}</td>
		</tr>
	@endforeach
</table>
{{ $cursos->links() }}
@include('footer')
@endsection('content')