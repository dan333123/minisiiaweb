@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Reporte-Alumno-Maestro")

@section('estilos_adicionales')
<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula Alumno</th>
        <th>No parcial</th>
        <th>Calificacion</th>
        <th>Estado</th>
        <th>Razón</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($alumnos as $alumno)
		<tr>
        <td>{{$alumno->MatriculaAlumno}}</td>
        <td>{{$alumno->noParcial}}</td>
        <td>{{$alumno->Calificacion}}</td>
        @if($alumno->Estado == 1)
        <td>Aprovado</td>
        @else
        <td>Reprovado</td>
        @endif
        <td>{{$alumno->Razon}}</td>
		</tr>
	@endforeach
</table>
@include('footer')
@endsection('content')