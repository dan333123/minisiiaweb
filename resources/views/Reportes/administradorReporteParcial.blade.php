@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Reporte-Administrador")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
    
@endsection('estilos_adicionales')

@section('content')
{{$a = 1}}
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Parcial</th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@for ($i=0;$i<$parciales;$i++)
		<tr>
		<td>{{$i+1}}</td>
		<td>
			<a href="{{ URL('/Administrador/Reporte/Actual/Cursos/'.$claveMateria,($i+1)) }}">Ver</a>
		</td>
		</tr>
	@endfor
</table>  
	@include('footer')
@endsection('content')