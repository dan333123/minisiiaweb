@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Alumno")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
    {{$contador = $resultado}}
	<form role="form" method="post" action="{{ url('/guardarParciales') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Alta de Parciales</legend>
			<p>
                @for($i=0;$i<$contador;$i++)
			<label for ="NoParcial">Parcial: {{$i+1}}</label>
				<br>
			<label for ="FechaInicio{{$i}}">Fecha de inicio</label>: <input type="date" name = "FechaInicio{{$i}}" id = "FechaInicio{{$i}}" size = "30" placeholder="Fecha de inicio" required><br/>
				<label for ="FechaFin{{$i}}">Fecha de fin</label>: <input type="date" name = "FechaFin{{$i}}" id = "FechaFin{{$i}}" size = "30" placeholder="Fecha de Fin de semestre" required><br/>
			   <hr>
				@endfor
				<input type= "hidden" name = "parciales" id = "parciales" size = "30" maxlength = "2"  value = "{{$contador}}">
				<input type= "hidden" name = "codigoSemestre" id = "codigoSemestre" size = "30" maxlength = "2"  value = "{{$codigoSemestre}}">
                <div class = "boton">
					<input type="submit" value="Enviar" ></code>
				</div>
			</p>
		</fieldset>
	</form>
	@include('footer')
@endsection('content')