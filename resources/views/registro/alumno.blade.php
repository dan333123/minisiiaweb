@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Alumno")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/guardarAlumno') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Datos Del Alumno</legend>
			<p>
				<label for ="matricula">Matricula Alumno</label>: <input type="text" name = "matricula" id = "matricula" size = "30" maxlength = "20" placeholder="Matricula del Alumno" autofocus required><br/> 
				<label for ="apellidoP">Apellido Paterno</label>: <input type="text" name = "apellidoP" id = "apellidoP" size = "30" maxlength = "20" placeholder="Apellido Paterno del Alumno" required><br/> 
				<label for ="apellidoM">Apellido Materno</label>: <input type="text" name = "apellidoM" id = "apellidoM" size = "30" maxlength = "20" placeholder="Apellido Materno del Alumno"><br/>
				<label for ="nombre">Nombre</label>: <input type="text" name = "nombre" id = "nombre" size = "30" maxlength = "15" placeholder="Nombre del Alumno" required><br/>
				<label for ="carrera">Carrera</label>: <input type="text" name = "carrera" id = "carrera"size = "30" maxlength = "20" placeholder="Carrera del alumno" required><br/>
				<label for ="telefono">Telefono</label>: <input type="tel" name = "telefono" id = "telefono"size = "30" maxlength = "10" placeholder="Telefono particular" required><br/>
				<label for ="email">Correo electronico</label>: <input type="email" name = "email" id ="email" size = "30" maxlength = "25" placeholder="Correo electronico" required><br/>
				<label for ="añoIngreso">Año de Ingreso</label>: <input type="numeric" name = "añoIngreso" id ="añoIngreso" size = "15" maxlength = "4" placeholder="Año de ingreso a la carrera" required><br/>
				<label for ="mesIngreso">Mes de Ingreso</label>: <input type="numeric" name = "mesIngreso" id ="mesIngreso" size = "15" maxlength = "2" placeholder="Mes de ingreso a la carrera" required><br/>
				<hr>
				<p>Genero:</p>
				<label for="mascu">Masculino</label><input type="radio" name="genero" value="Masculino" id="mascu" checked/><br/>
				<label for="feme">Femenino</label><input type="radio" name="genero" value="Femenino" id="feme" /><br/>
				<hr>
				<div class = "boton">
					<input type="submit" value="Enviar" ></code>	
				</div>
			</p>
		</fieldset>
	</form>
    <div class="resultado">
		<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')
