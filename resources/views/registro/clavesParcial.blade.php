@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Claves-Parciales")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
<div class="resultado">
    <p>{{$resultado}}</p>
</div>
	<form role="form" method="post" action="{{ url('/claveParcial') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Generar clave Parcial</legend>
			<p>
				<label for ="claveMateria">Clave Materia</label>: <input type="text" name = "claveMateria" id = "claveMateria" size = "30" maxlength = "20" placeholder="Clave de la materia" autofocus required><br/> 
				<label for ="noParcial">Numero del parcial</label>: <input type="numeric" name = "noParcial" id = "noParcial" size = "30" maxlength = "2" placeholder="Numero del parcial" required><br/> 
				<label for ="matriculaMaestro">Matricula del maestro</label>: <input type="text" name = "matriculaMaestro" id = "matriculaMaestro" size = "30" maxlength = "20" placeholder="Matricula del Maestro"><br/>
				<label for ="matriculaAlumno">Matricula del alumno</label>: <input type="text" name = "matriculaAlumno" id = "matriculaAlumno" size = "30" maxlength = "15" placeholder="Matricula del Alumno" required><br/>
				<label for ="descripcion">Descripcion</label>: <input type="text" name = "descripcion" id = "descripcion"size = "30" maxlength = "80" placeholder="Descripcion o casusa"><br/>
				<div class = "boton">
					<input type="submit" value="Enviar" ></code>	
				</div>
			</p>
		</fieldset>
	</form>
	@include('footer')
@endsection('content')