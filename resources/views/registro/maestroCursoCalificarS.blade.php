@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Calificar-Alumno")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/calificarAlumno') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Calificar Alumno</legend>
			<p>
				<label for ="Calificacion">Calificacion</label>: <input type="number" name = "Calificacion" id = "Calificacion" size = "30" maxlength = "3" placeholder="Calificacion del Alumno"  autofocus required><br/> 
				<label for ="Faltas">Faltas</label>: <input type="number" name = "Faltas" id = "Faltas" size = "30" maxlength = "3" placeholder="Faltas del Alumno" required><br/> 
				<hr>
				<p>Estado:</p>
                    <label for="apro">Aprovado</label><input type="radio" name="aprovado" value="1" id="apro" checked/><br/>
				    <label for="repro">Reprobado</label><input type="radio" name="aprovado" value="0" id="repro" /><br/>
				<hr>
                <p>Razon:</p>
                    <label for="aprov">Aprovado</label><input type="radio" name="razon" value="Aprovado" id="aprov" checked/><br/>
                    <label for="fal">Faltas</label><input type="radio" name="razon" value="Faltas" id="fal" /><br/>
                    <label for="cal">Calificación</label><input type="radio" name="razon" value="Calificación" id="cal" /><br/>
                    <label for="des">Desercion</label><input type="radio" name="razon" value="Desercion" id="des" /><br/>
				<hr>
                <p>Acentada:</p>
                    <label for="acentada">Acentada</label><input type="radio" name="acentado" value="1" id="acentada"/><br/>
                    <label for="acentadaNo">No acentada</label><input type="radio" name="acentado" value="0" id="acentadaNo" checked/><br/>
				<hr>
                <input type= "hidden" name = "MatriculaAlumno" id = "MatriculaAlumno"   value = "{{$MatriculaAlumno}}">
                <input type= "hidden" name = "curso" id = "curso"   value = "{{$curso}}">
                <input type= "hidden" name = "claveMateria" id = "claveMateria"   value = "{{$claveMateria}}">
            	<div class = "boton">
					<input type="submit" value="Enviar" ></code>	
				</div>

			</p>
		</fieldset>
	</form>
    <div class="resultado">
		<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')