@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Alumno")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/guardarSemestre') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Alta de Semestre</legend>
			<p>
				<label for ="Campus">Campus</label>: <input type="text" name = "Campus" id = "Campus" size = "20" maxlength = "15" placeholder="Campus del semestre" autofocus required><br/> 
				<label for ="NoParciales">No. de parciales</label>: <input type="numeric" name = "NoParciales" id = "NoParciales" size = "30" maxlength = "2" placeholder="Numero de parciales del semstre" value = "3" required><br/> 
				<label for ="CalifAprovatoria">Calificacion aprovatoria</label>: <input type="numeric" name = "CalifAprovatoria" id = "CalifAprovatoria" size = "30" maxlength = "3" placeholder="Calificacion aprovatoria del semestre" value = "70" required><br/>
				<label for ="FechaInicio">Fecha de inicio</label>: <input type="date" name = "FechaInicio" id = "FechaInicio" size = "30" placeholder="Fecha de inicio" required><br/>
				<label for ="FechaFin">Fecha de fin</label>: <input type="date" name = "FechaFin" id = "FechaFin" size = "30" placeholder="Fecha de Fin de semestre" required><br/>
                <hr>
                <div class = "boton">
					<input type="submit" value="Siguiente" ></code>
				</div>
			</p>
		</fieldset>
	</form>
	@include('footer')
@endsection('content')