@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Alta-Curso-Alumnos")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
@endsection('estilos_adicionales')
<div class="resultado">
	<p>{{$resultado}}</p>
</div>
@section('content')
	<form role="form" method="post" action="{{ url('/guardarCursoAlumno') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Datos Del Alumno </legend>
			<p>
				<label for ="matricula">Matricula Alumno</label>: <input type="text" name = "matricula" id = "matricula" size = "30" maxlength = "20" placeholder="Matricula del Alumno" autofocus required><br/> 
				<label for ="semestreA">Semestre actual</label>: <input type="numeric" name = "semestreA" id = "semestreA" size = "30" maxlength = "2" placeholder="Semestre Actual del alumno" required><br/> 
				<label for="curso">Tipo de Curso</label><br/>
                <select name="curso" id="curso">
                        <option value="Normal">Normal</option>
						<option value="Repeticion">Repeticion</option>
						<option value="Especial">Especial</option>
				</select> 
				<input type= "hidden" name = "claveMateria" id = "claveMateria"  value = "{{$claveMateria}}">
				<hr>
				<div class = "boton">
					<input type="submit" value="Enviar" ></code>	
				</div>
			</p>
		</fieldset>
	</form>

	<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
		<thead>
		<tr>
			<th>Matricula Alumno</th>
			<th>Nombre</th>
			<th>Carrera </th>
		</tr>
		</thead>
		<tbody>
		@foreach ($alumno as $alumnos)
			<tr>
			<td>{{$alumnos->MatriculaAlumno}}</td>
			<td>{{$alumnos->nombre}}</td>
			<td>{{$alumnos->Carrera}}</td>
			</tr>
		@endforeach
	</table>
	{{ $alumno->links() }}	
	@include('footer')
@endsection('content')