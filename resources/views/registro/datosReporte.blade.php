@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Registro-Alumno")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/datosDelInforme') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Datos Del Informe</legend>
			<p>
				<label for ="parcial">Parcial</label>: <input type="numeric" name = "parcial" id = "parcial" size = "30" maxlength = "2" placeholder="Parcial del reporte" autofocus required><br/> 
				<label for ="uProgramadas">Unidades calificadas</label>: <input type="numeric" name = "uProgramadas" id = "uProgramadas" size = "30" maxlength = "2" placeholder="Unidades programadas para el parcial" required><br/> 
				<label for ="uCubiertas">Unidades cubiertas</label>: <input type="numeric" name = "uCubiertas" id = "uCubiertas" size = "30" maxlength = "2" placeholder="Unidades cubiertas a la fecha" required><br/> 
				<label for ="uDemas">Unidades evaludas, calificasas, entregadas</label>: <input type="text" name = "uDemas" id = "uDemas" size = "30" maxlength = "3" placeholder="" required><br/>
				<hr>
				<div class = "boton">
					<input type="submit" value="Enviar" ></code>	
				</div>
			</p>
		</fieldset>
	</form>
    <div class="resultado">
		<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')
