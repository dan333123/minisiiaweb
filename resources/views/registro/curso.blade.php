@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Curso")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/guardarCurso') }}">
		{!! csrf_field() !!}
		<fieldset>
			<legend>Datos Del Curso</legend>
			<p>
                <label for="materia">Materia</label>: <input type="text" name = "materia" id = "materia" size = "30" maxlength = "40" placeholder="Nombre Materia" value = "{{$nombre}}" disabled><br/> 
				<label for ="grupo">Grupo</label>: <input type="text" name = "grupo" id = "grupo" size = "30" maxlength = "2" placeholder="Grupo" required><br/> 
				<label for ="salon">Salon</label>: <input type="text" name = "salon" id = "salon" size = "30" maxlength = "8" placeholder="Salon del curso" required><br/> 
				<label for="maestro">Maestro</label><br/>
                <select name="maestro" id="maestro">
                @foreach($maestro as $maestros)
                        <option value="{{$maestros->matriculaMaestro}}">{{$maestros->nombreM}}</option>
                @endforeach
            </select>  
            <input type= "hidden" name = "clave" id = "clave" size = "30" maxlength = "20" value = "{{$clave}}">
				<hr>
				<div class = "boton">
					<input type="submit" value="Enviar" ></code>	
				</div>
			</p>
		</fieldset>
	</form>
	    <div class="resultado">
		<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')