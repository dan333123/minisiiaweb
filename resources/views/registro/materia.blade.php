@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Alta Materia")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/guardarMateria') }}">
		{!! csrf_field() !!} <!-- csrf = cross-site request forgery (para evitar ataques de peticiones post al servidor y permitir a laravel la consulta post, aunque es posible desactivar el csrf en el archivo Kernerl.php)-->
		<fieldset>
			<legend>Registro de Materia</legend>
			@include('partes.formularioMateria')
			<div class = "boton">
				<input type="submit" value="Enviar" ></code>	
			</div>
		</fieldset>
	</form>
	<div class="resultado">
		<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')

