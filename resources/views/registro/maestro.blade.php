@include('partes.imagenCor');
@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Registro-Maestro")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/guardarMaestro') }}">
		{!! csrf_field() !!} <!-- csrf = cross-site request forgery (para evitar ataques de peticiones post al servidor y permitir a laravel la consulta post, aunque es posible desactivar el csrf en el archivo Kernerl.php)-->
		<fieldset>
			<legend>Datos del Maestro</legend>
			@include('partes.formularioMaestro')
			<div class = "boton">
				<input type="submit" value="Enviar" ></code>	
			</div>
		</fieldset>
	</form>
	<div class="resultado">
		<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')

