@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.alumnoMain');
@section('title', "Registro-Maestro")

@section('estilos_adicionales')
    
@endsection('estilos_adicionales')

@section('content')
<script src = "{{ asset('js/Chart.js') }}"> </script>
    <script>
        var nombre;
        var index=0;
        var calificacion=new Array();
        var parcial=new Array();
    </script>
    @foreach($calificaciones as $calificacion)
        <script>
            nombre="{{$calificacion->nombre}}";
            calificacion.push({{$calificacion->calificacion}});
            parcial.push({{$calificacion->NoParcial}});
            index+=1;
        </script>
    @endforeach
    <center>
    <div class="graphic" style="width:50%;height:40%;">
        <canvas id="chart"></canvas>
    </div>
    </center>
    <script>
        var ctx = document.getElementById("chart").getContext('2d');
        var myBarChart;
        var propiedades=new Array();
        var etiquetas=new Array();
        var enteros=new Array();
        etiquetas.push(nombre);
        propiedades.push({
                    data:[noAlumnos],
                    backgroundColor:"green",
                    borderwidth:1,
                    borderColor:"black",
                    label:"Calificacion: "+calificacion[i]
                });
		for(i=0;i<index;i++){
            if(calificacion[i]>6){
                propiedades.push({
                    data:[parcial[i]],
                    backgroundColor:"green",
                    borderwidth:1,
                    borderColor:"black",
                    label:"Calificacion: "+calificacion[i]
                });
            }else{
                propiedades.push({
                    data:[parcial[i]],
                    backgroundColor:"red",
                    borderwidth:1,
                    borderColor:"black",
                    label:"Calificacion: "+calificacion[i]
                });
            }
            enteros.push(i+1);
        }
        var dataChart = {
            labels:etiquetas,
            datasets:propiedades
        };
        var options = {
            scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize:1
                }
                }],
                xAxes: [{
                    maxBarThickness: 100,
                    ticks:{
                        fontSize:20
                    }
                }],
            }, 
            title: {
                fontSize: 18,
                display: true,
                text: "Parcial",
                position: 'left'
            },
            legend: {
                position:"top"
            }
        };
        window.onload=function(){
            myBarChart = new Chart(ctx, {
                type: 'bar',
                data: dataChart,
                options: options
            });
            myBarChart.update();
        };
    </script>
@include('footer')
@endsection('content')