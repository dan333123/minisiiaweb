@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.alumnoMain');
@section('title', "Registro-Maestro")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
    <link rel = "stylesheet" href = "{{ asset('css/FormularioBusqueda.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
		<th>Clave</th>
		<th>Nombre</th>
		<th>No Unidades</th>
        <th></th>
	</tr>
	</thead>
	<tbody>
	@foreach ($materias as $materia)
		<tr>
		<td>{{$materia->ClaveMateria}}</td>
		<td>{{$materia->nombre}}</td>
		<td>{{$materia->Unidades}}</td>
		<td>
			<a href="{{ URL('/calificacion',$materia->ClaveMateria) }}">Ver</a>
		</td>
		</tr>
	@endforeach
</table>

@include('footer')
@endsection('content')