<!DOCTYPE HTML>
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="Index" content=" Administración"/>
		<title>@yield('title')- Mini Siia</title>
		<link rel="shortcut icon" href="{{ asset('images/icons/head_icon2.ico') }}"/>
		<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Arvo:700' rel='stylesheet' type='text/css'>
		<link rel = "stylesheet" href = "{{ asset('css/fonts.css') }}"> </link> 
		<link rel = "stylesheet" href = "{{ asset('css/main.css') }}"> </link>
		<link rel = "stylesheet" href = "{{ asset('css/Fondo.css') }}"> </link>
		<script src = "{{ asset('js/jquery.js') }}"> </script>
        @yield('estilos_adicionales')

	</head>

	<body>
	<script>
		$( document ).ready(function() {/* Implementa los scripts y estilos antes de mostrarlos, de esa manera
											se muestra correctamente el contenido, por ejemplo: el menu
											para administrador al refrescar se desplazaba, esto es corregido
											con esta parte de JQuery */
			console.log( "It's Done!" );
		});
	</script>
    <div id="content">
	@yield('content')
	</div>
    <!-- Los scrips al final para que la pagina cargue más rápido -->
    
	</body>
</html>