@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Grafica")

@section('content')
<script src = "{{ asset('js/Chart.js') }}"> </script>
    <script>
        /* Variables */
        var noAlumnos=0;
        var desertores=0;
        var cont1=new Array();
        var cont2=new Array();
        var cont3=new Array();
        var porcentaje;
        var calificacion=new Array();
        var parcial=new Array();
        var razon=new Array ();
        var maxParcial=0;
    </script>
    @foreach($noAlumnos as $noAlumno)
        <script>
            noAlumnos+=1;
        </script>
    @endforeach
    @foreach($calificaciones as $calificacion)
        <script>
            calificacion.push({{$calificacion->calificacion}});
            parcial.push({{$calificacion->NoParcial}});
            razon.push("{{$calificacion->razon}}");
        </script>
    @endforeach
    <center>
    <div class="graphicContent" style="display:flex;">
        <div class="graphic" style="width:80%;">
            <canvas id="chart"></canvas>
        </div>
        <div style="width:30%;" id="aditionalInf"></div>
    <div>
    </center>
    <script>
        var ctx = document.getElementById("chart").getContext('2d');
        var myBarChart;
        var propiedades=new Array();
        var etiquetas=new Array();
        var content="";
        var contentInf;
        /* Obtenemos valores de contadores para posteriormente generar porcentajes */
        for(i=0;i<parcial.length;i++){
            if(parcial[i]>maxParcial){
                maxParcial=parcial[i];
            }
        }
        porcentaje=(100/noAlumnos);
        for(i=0;i<maxParcial;i++){
            cont1.push(0);
            cont2.push(0);
            cont3.push(0);
            for(u=0;u<calificacion.length;u++){ /* Hace sumatoria por parcial*/
                if(parcial[u]==(i+1)){
                    if(calificacion[u]>6 && razon[u]=="Aprovado"){
                        cont1[i]+=1; /* <--- Aprobados */
                    }else if(calificacion[u]<7 && razon[u]=="Reprobado"){
                        cont2[i]+=1; /* <--- Reprobados */
                    }else if(razon[u]=="Desercion"){
                        cont2[i]+=1;
                        cont3[i]+=1; /* <--- Desertores */
                    }
                }
            }
            if((cont2[i]+cont3[i])>0){
                desertores=(100/(cont2[i])*cont3[i])
            }else{desertores=0;}
            content=content+"<div><h4>Parcial "
            +(i+1)+"</h4>Aprobados:"+((cont1[i])*porcentaje).toFixed(2)+"%   Reprobados:"
            +((cont2[i])*porcentaje).toFixed(2)+"%   Desertores:"+desertores.toFixed(2)+"%</div>";

            etiquetas.push("Parcial "+(i+1));
        }
        contentInf=document.getElementById("aditionalInf");
        contentInf.innerHTML=content;       
                propiedades.push({
                    data:[noAlumnos],
                    backgroundColor:"white",
                    label:""
                });
                propiedades.push({
                    data:cont1,
                    backgroundColor:"green",
                    label:"Aprobados",
                    borderWidth:1,
                    borderColor:"black",
                });
                propiedades.push({
                    data:cont2,
                    backgroundColor:"red",
                    label:"Reprobados",
                    borderWidth:1,
                    borderColor:"black",
                });
                propiedades.push({
                    data:cont3,
                    backgroundColor:"pink",
                    label:"Desertores",
                    borderWidth:1,
                    borderColor:"black",
                });
        var dataChart = {
            labels:etiquetas,
            datasets:propiedades
        };
        var options = {
            scales: {
                yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize:1
                }
                }],
                xAxes: [{
                    maxBarThickness: 100,
                    ticks:{
                        fontSize:20
                    }
                }],
            }, 
            title: {
                fontSize: 18,
                display: true,
                text: "No Alumnos",
                position: 'left'
            },
            legend: {
                position:"top"
            }
        };
        window.onload=function(){
            myBarChart = new Chart(ctx, {
                type: 'bar',
                data: dataChart,
                options: options
            });
            myBarChart.update();
        };
    </script>
@include('footer')
@endsection('content')