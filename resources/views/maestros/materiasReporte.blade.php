@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Registro-Maestro")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
    <link rel = "stylesheet" href = "{{ asset('css/FormularioBusqueda.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
		<th>Clave</th>
		<th>Grupo</th>
		<th>Salon</th>
		<th>Nombre</th>
		<th>Unidades</th>
        <th>Reporte</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($curso as $materia)
		<tr>
		<td>{{$materia->ClaveMateria}}</td>
		<td>{{$materia->Grupo}}</td>
		<td>{{$materia->Salon}}</td>
		<td>{{$materia->Nombre}}</td>
		<td>{{$materia->Unidades}}</td>
		<td>
			<a href="{{ URL('/maestro/reporte/'.$materia->ClaveMateria,$claveProfe)}}">Generar</a>
		</td>
		</tr>
	@endforeach
</table>

@include('footer')
@endsection('content')