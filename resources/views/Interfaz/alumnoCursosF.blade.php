@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.alumnoMain');

@section('title', "Cursos-Alumnos")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave Materia</th>
        <th>Nombre </th>
		<th>Grupo</th>
		<th>Salon</th>
        <th>Calificacion</th>
		<th>Estado</th>
		<th>Razon</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($materia as $materias)
		<tr>
		<td>{{$materias->claveMateria}}</td>
		<td>{{$materias->nombre}}</td>
		<td>{{$materias->Grupo}}</td>
		<td>{{$materias->Salon}}</td>
		<td>{{$materias->Calificacion}}</td>
		@if($materias->Estado == 1)
			<td>Aprobado</td>
			@else 
			<td>Reprobado</td>
		@endif
		<td>{{$materias->Razon}}</td>
		</tr>
	@endforeach
</table>
{{ $materia->links() }}
@include('footer')
@endsection('content')