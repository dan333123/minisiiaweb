@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Alta-Curso")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave</th>
        <th>Nombre</th>
        <th>Carrera </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($materia as $materias)
		<tr>
		<td>{{$materias->clave}}</td>
		<td>{{$materias->nombre}}</td>
		<td>{{$materias->carrera}}</td>
		<td>
			<a href="{{ URL('/alta/curso',$materias->clave) }}">Alta</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $materia->links() }}
@include('footer')
@endsection('content')