@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Alumnos-Actuales")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
    
@endsection('estilos_adicionales')

@section('content')
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave Materia</th>
        <th>Nombre</th>
        <th>Grupo </th>
        <th>Salon </th>
        <th>Matricula alumno </th>
        <th>Nombre alumno </th>
		<th>Accion</th>
		<th>Reporte</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($curso as $cursos)
		<tr>
		<td>{{$cursos->ClaveMateria}}</td>
		<td>{{$cursos->nombre}}</td>
		<td>{{$cursos->grupo}}</td>
		<td>{{$cursos->salon}}</td>
        <td>{{$cursos->matriculaAlumno}}</td>
        <td>{{$cursos->nombreC}}</td>
		<td>
			<a href="{{ URL('/maestro/ver/alumnos/'.$cursos->ClaveMateria,$cursos->matriculaAlumno) }}">Ver</a>
		</td>
		<td>
			<a href="{{ URL('/maestro/reporte/{materia}'.$cursos->ClaveMateria) }}">Generar</a>
		</td>
		</tr>
	@endforeach
</table>   
	@include('footer')
@endsection('content')