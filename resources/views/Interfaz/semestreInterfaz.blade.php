@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Registro-Alumno")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/InterfazSemestre.css') }}"> </link>
    
@endsection('estilos_adicionales')

@section('content')
    <div class = "Contenedor">
        <article>
            <legend>Semestre Actual</legend>
            <hr>
            @foreach($semestre as $semestres)
                <h2>Codigo Semestre: </h2>
                <p>{{$semestres->CodigoSemestre}}</p>
                <h2>Campus:</h2>
                <p>{{$semestres->Campus}}</p>
                <h2>Fecha de inicio: </h2>
                <p>{{$semestres->FechaInicio}}</p>
                <h2>Fecha de fin: </h2>
                <p>{{$semestres->FechaFin}}</p>
                <h2>Caificacion aprovatoria: </h2>
                <p>{{$semestres->CalifAprovatoria}}</p>
                <h2>Numero de parciales: </h2>
                <p>{{$semestres->NoParciales}}</p>
            @endforeach
            <input type="button" value="Mostrar Parciales" onclick="mostrar()"> 
            <div class = "Parciales">
                    @foreach($parcial as $parciales)
                    <h2>No. Parcial: </h2>
                    <p>{{$parciales->NoParcial}}</p>
                    <h2>Fecha de inicio:</h2>
                    <p>{{$parciales->FechaInicio}}</p>
                    <h2>Fecha de fin: </h2>
                    <p>{{$parciales->FechaFin}}</p>
                    <hr>
                @endforeach
                
            </div>    
        </article>    
    </div>    
    @foreach($semestre as $semestres)
    <div class = "Boton">
            <a href="{{ URL('/semestre/eliminar',$semestres->CodigoSemestre) }}">Finalizar Semestre</a>
    </div>
    @endforeach
    
	@include('footer')
@endsection('content')