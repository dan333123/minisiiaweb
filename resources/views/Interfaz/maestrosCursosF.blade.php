@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Cursos-Calificar-Final")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
    
@endsection('estilos_adicionales')

@section('content')
<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave Materia</th>
        <th>Nombre</th>
        <th>Grupo </th>
        <th>Salon </th>
		<th>Unidades </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($curso as $cursos)
		<tr>
		<td>{{$cursos->ClaveMateria}}</td>
		<td>{{$cursos->Nombre}}</td>
		<td>{{$cursos->Grupo}}</td>
		<td>{{$cursos->Salon}}</td>
		<td>{{$cursos->Unidades}}</td>
		<td>
			<a href="{{ URL('/maestro/cursos/calificar/final',$cursos->ClaveMateria) }}">Ver</a>
		</td>
		</tr>
	@endforeach
</table> 
{{ $curso->links() }}    
	@include('footer')
@endsection('content')