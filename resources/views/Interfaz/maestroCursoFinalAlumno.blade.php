@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.maestroMain');
@section('title', "Cursos-Calificar")

@section('estilos_adicionales')
    <link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>ç
	<link rel = "stylesheet" href = "{{ asset('css/FormularioBusqueda.css') }}"> </link>
    
@endsection('estilos_adicionales')

@section('content')
<form id = "Busqueda" role="form" method="post" action="{{ url('/reactivarCalifFinal') }}">
{!! csrf_field() !!}
<legend>Reactivar calificación</legend>
<p>
    <label for ="clave">Clave:</label> 
	<input type="text" name = "clave" id = "clave" size = "30" maxlength = "20" placeholder="Clave unica por alumno" required><br/>
	<input type= "hidden" name = "claveMateria" id = "claveMateria" value = "{{$claveMateria}}">
</p>
</form>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula</th>
		<th>Nombre</th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
		@foreach ($alumno as $alumnos)
		<tr>
		<td>{{$alumnos->MatriculaAlumno}}</td>
		<td>{{$alumnos->nombre}}</td>
		<td>
			<a href="{{ URL('/maestro/cursos/calificar/final/'.$claveMateria,$alumnos->MatriculaAlumno) }}">Calificar</a>
		</td>
		</tr>
	@endforeach
</table>  
	@include('footer')
@endsection('content')