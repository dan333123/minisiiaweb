@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Alta-Curso-Alumnos")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave Curso</th>
        <th>Nombre Materia</th>
        <th>Carrera </th>
		<th>Grupo</th>
		<th>Salon</th>
		<th>Maestro</th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($materia as $materias)
		<tr>
		<td>{{$materias->ClaveMateria}}</td>
		<td>{{$materias->nombre}}</td>
		<td>{{$materias->Carrera}}</td>
		<td>{{$materias->Grupo}}</td>
		<td>{{$materias->Salon}}</td>
		<td>{{$materias->nombreM}}</td>
		<td>
			<a href="{{ URL('/asignar/curso',$materias->ClaveMateria) }}">Alta</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $materia->links() }}
@include('footer')
@endsection('content')