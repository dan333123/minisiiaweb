<p>
    <label for ="matricula">Matricula Maestro</label>: <input type="text" name = "matricula" id = "matricula" size = "30" maxlength = "20" placeholder="Matricula del maestro" autofocus required><br/> 
    <label for ="apellidoP">Apellido Paterno</label>: <input type="text" name = "apellidoP" id = "apellidoP" size = "30" maxlength = "20" placeholder="Apellido Paterno del maestro" required><br/> 
    <label for ="apellidoM">Apellido Materno</label>: <input type="text" name = "apellidoM" id = "apellidoM" size = "30" maxlength = "20" placeholder="Apellido Materno del maestro"><br/>
    <label for ="nombre">Nombre</label>: <input type="text" name = "nombre" id = "nombre" size = "30" maxlength = "15" placeholder="Nombre del maestro" required><br/>
    <label for ="departamento">Departamento</label>: <input type="text" name = "departamento" id = "departamento"size = "30" maxlength = "40" placeholder="Departamento del maestro" required><br/>
    <label for ="telefono">Telefono</label>: <input type="tel" name = "telefono" id = "telefono"size = "30" maxlength = "10" placeholder="Telefono particular" required><br/>
    <label for ="email">Correo electronico</label>: <input type="email" name = "email" id ="email" size = "30" maxlength = "25" placeholder="Correo electronico" required><br/>
    <hr>
    <p>Genero:</p>
    <label for="mascu">Masculino</label><input type="radio" name="genero" value="Masculino" id="mascu" checked/><br/>
    <label for="feme">Femenino</label><input type="radio" name="genero" value="Femenino" id="feme" /><br/>
    <hr>
</p>