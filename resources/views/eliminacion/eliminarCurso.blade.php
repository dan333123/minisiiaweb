@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Asignacion-Curso")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<div class="resultado">
		<p>{{$resultado}}</p>
</div>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave Materia</th>
        <th>Nombre</th>
        <th>Nombre maestro </th>
        <th>Grupo </th>
		<th>Salon </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($cursos as $curso)
		<tr>
		<td>{{$curso->ClaveMateria}}</td>
		<td>{{$curso->Nombre}}</td>
		<td>{{$curso->nombreC}}</td>
		<td>{{$curso->Grupo}}</td>
		<td>{{$curso->Salon}}</td>
		<td>
			<a href="{{ URL('/curso/ver',$curso->ClaveMateria) }}">Ver</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $cursos->links() }}
@include('footer')
@endsection('content')