@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Registro-Maestro")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<div class="resultado">
		<p>{{$resultado}}</p>
</div>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula</th>
        <th>Nombre</th>
        <th>Carrera </th>
        <th>Correo </th>
		<th>Telefono </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($alumnos as $alumno)
		<tr>
		<td>{{$alumno->MatriculaAlumno}}</td>
		<td>{{$alumno->nombre}}</td>
		<td>{{$alumno->Carrera}}</td>
		<td>{{$alumno->correo}}</td>
		<td>{{$alumno->telefono}}</td>
		<td>
			<a href="{{ URL('/alumno/editar',$alumno->MatriculaAlumno) }}">Editar</a>
			<a href="{{ URL('/alumno/eliminar',$alumno->MatriculaAlumno) }}">Eliminar</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $alumnos->links() }}
@include('footer')
@endsection('content')