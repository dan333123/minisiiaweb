@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Registro-Maestro")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<div class="resultado">
		<p>{{$resultado}}</p>
</div>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Clave</th>
        <th>Nombre</th>
        <th>Carrera </th>
        <th>Unidades </th>
		<th>Usuario </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($materias as $materia)
		<tr>
		<td>{{$materia->clave}}</td>
		<td>{{$materia->nombre}}</td>
		<td>{{$materia->carrera}}</td>
		<td>{{$materia->unidades}}</td>
		<td>{{$materia->id}}</td>
		<td>
			<a href="{{ URL('/materia/editar',$materia->clave) }}">Editar</a>
			<a href="{{ URL('/materia/eliminar',$materia->clave) }}">Eliminar</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $materias->links() }}
@include('footer')
@endsection('content')