@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');

@section('title', "Registro-Maestro")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/tablaDatos.css') }}"> </link>
	<link rel = "stylesheet" href = "{{ asset('css/paginacion.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')

<div class="resultado">
		<p>{{$resultado}}</p>
</div>

<table id="tablaAvisos" cellpadding = "0" cellspacing="0">
	<thead>
	<tr>
        <th>Matricula</th>
        <th>Nombre</th>
        <th>Departamento </th>
        <th>Correo </th>
		<th>Telefono </th>
		<th>Accion</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($maestros as $maestro)
		<tr>
		<td>{{$maestro->matriculaMaestro}}</td>
		<td>{{$maestro->nombre}}</td>
		<td>{{$maestro->Departamento}}</td>
		<td>{{$maestro->correo}}</td>
		<td>{{$maestro->telefono}}</td>
		<td>
			<a href="{{ URL('/maestro/editar',$maestro->matriculaMaestro) }}">Editar</a>
			<a href="{{ URL('/maestro/eliminar',$maestro->matriculaMaestro) }}">Eliminar</a>
		</td>
		</tr>
	@endforeach
</table>
{{ $maestros->links() }}
@include('footer')
@endsection('content')