<style>
header nav ul li ul{ /*Hover No 1*/
	width:14.28%;
}
header nav > ul > li > a > span{
	width:14.28%;
}
</style>
<header>
			<nav>
				<ul>
					<li class="firstElement"><a href="{{ url('/home') }}"><span class="primero"><i class="icon icon-home"></i></span>Archivo</a>
						<ul class="submenu">
							<li>
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Cerrar Sesion
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
					<li class="firstElement"><a href=""><span class="segundo"><i class="icon icon-cog"></i></span>Alumnos </a>
						<ul class="submenu">
							<li><a href = "{{ url('/maestro/ver/cursos/alumnos') }}">Ver Alumnos<a/></li><!-- Pendiente -->
							<li>Ver calificaciones<a/>
							<ul class="submenuSubmenu">
								<li><a href = "{{ url('/maestro/ver/alumnos') }}">
									Mis alumnos
								</li>
								<li><a href = "{{ url('/maestro/cursos') }}">
									Mis cursos
								</li>
							</ul>
							</li><!-- Pendiente -->
						</ul>
					</li>
					<li class="firstElement"><a href=""><span class="tercero"><i class="icon icon-books"></i></span>Cursos</a>
						<ul class="submenu">
							
							<li><a href = "{{ url('/maestro/cursos/actuales') }}">Cursos actuales<a/></li>
							<li><a href = "{{ url('/maestro/cursos/calificar') }}">Calificar parciales<a/></li>
							<li><a href = "{{ url('/maestro/cursos/calificar/final') }}">Calificar final<a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="cuarto"><i class="icon icon-user-tie"></i></span>Reportes</a>
						<ul class="submenu">
							<li><a href = "{{ url('maestro/reporte') }}">Generar reportes<a/></li><!-- Pendiente -->
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="quinto"><i class="icon icon-user"></i></span>Extras</a>
						<ul class="submenu">
							<li><a href = "{{ url('') }}">Avisos<a/></li><!-- Pendiente -->
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="noveno"><i class="icon icon-user"></i></span>Ayuda</a>
						<ul class="submenu">
							<li><a href = "{{ url('') }}">Acerca de<a/></li><!-- Pendiente -->
						</ul>
					</li>
				</ul>
			</nav>
		</header>