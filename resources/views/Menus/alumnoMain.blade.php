<style>
header nav ul li ul{ /*Hover No 1*/
	width:14.28%;
}
header nav > ul > li > a > span{
	width:14.28%;
}
</style>
<header>
			<nav>
				<ul>
					<li class="firstElement"><a href="{{ url('/home') }}"><span class="primero"><i class="icon icon-home"></i></span>Archivo</a>
						<ul class="submenu">
							<li>
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Cerrar Sesion
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
					<li class="firstElement"><a href=""><span class="segundo"><i class="icon icon-cog"></i></span>Cursos </a>
						<ul class="submenu">
							<li><a href = "{{ url('/alumno/cursos/actuales') }}">Cursos actuales<a/></li><!-- Pendiente -->
							<li><a href = "{{ url('/materias') }}">Ver calificaciones<a/></li><!-- Pendiente -->
						</ul>
					</li>
					<li class="firstElement"><a href=""><span class="tercero"><i class="icon icon-books"></i></span>Semestre</a>
						<ul class="submenu">
							<li><a href = "file:algo.html">Parcial<a/></li><!-- Pendiente -->
							<li><a href = "{{ url('/alumno/cursos/actuales/final') }}">Mostrar final<a/></li><!-- Pendiente -->
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="cuarto"><i class="icon icon-user-tie"></i></span>Ayuda</a>
						<ul class="submenu">
							<li><a href = "{{ url('') }}">Acerca de<a/></li><!-- Pendiente -->
						</ul>
					</li>
				</ul>
			</nav>
		</header>