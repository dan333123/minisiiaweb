<style>
header nav ul li ul{ /*Hover No 1*/
	width:12.5%;
}
header nav > ul > li > a > span{
	width:12.5%;
}
</style>
<header>
			<nav>
				<ul>
					<li class="firstElement"><a href="{{ url('/home') }}"><span class="primero"><i class="icon icon-home"></i></span>Archivo</a>
						<ul class="submenu">
							<li>
								<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									Cerrar Sesion
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
					<li class="firstElement"><a href=""><span class="segundo"><i class="icon icon-cog"></i></span>Administrar </a>
						<ul class="submenu">
							<li><a href = "{{ url('/Administrar/semestre') }}">Semestre<a/></li>
							<li><a href = "{{ url('/home') }}">Recordatorios<a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href=""><span class="tercero"><i class="icon icon-books"></i></span>Materias</a>
						<ul class="submenu">
							<li><a href = "{{ url('/registro/materia') }}">Alta Materia<a/></li>
								<li><a href = "{{ url('/materia/eliminar') }}">Baja/Modificacion Materia<a/></li>
							<li><a href = "{{ url('/registro/curso') }}">Alta Curso<a/></li>
								<li><a href = "{{ url('/curso/asignar') }}">Asignar alumnos a cursos<a/></li>
								<li><a href = "{{ url('/curso/ver') }}">Asignaciones de Cursos<a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="cuarto"><i class="icon icon-user-tie"></i></span>Maestros</a>
						<ul class="submenu">
							<li><a href = "{{ url('/registro/maestro') }}">Alta Maestro<a/></li>
							<li><a href = "{{ url('/maestro/eliminar') }}">Baja/Modificacion Maestro<a/></li>
							<li><a href = "{{ url('/maestro/buscar') }}">Busqueda Maestro<a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="quinto"><i class="icon icon-user"></i></span>Alumnos</a>
						<ul class="submenu">
							<li><a href = "{{ url('/registro/alumno') }}">Alta Alumno<a/></li>
							<li><a href = "{{ url('/alumno/eliminar') }}">Baja/Modificacion Alumno<a/></li>
							<li><a href = "{{ url('/alumno/buscar') }}">Busqueda Alumno<a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="sexto"><i class="icon icon-list2"></i></span>Extras</a>
						<ul class="submenu">
							<li><a href = "{{ url('/extras/califParcial') }}">Modificar Calificacion parcial<a/></li>
							<li><a href = "{{ url('/extras/califFinal') }}">Modificar Calificacion final<a/></li>
							<li><a href = "{{ url('/extras/usuarios') }}">Usuarios <a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="segundo"><i class="icon icon-list2"></i></span>Reportes</a>
						<ul class="submenu">
							<li><a href ="{{ url('/reporte/SemestreActual') }}">Semestre actual<a/></li>
							<li><a href ="{{ url('/reporte/Semestre') }}">Semestres antiguos <a/></li>
						</ul>
					</li>
					<li class="firstElement"><a href="file:algo.html"><span class="septimo"><i class="icon icon-link"></i></span>Ayuda</a>
						<ul class="submenu">
							<li><a href = "file:algo.html">Acerca de<a/></li>
						<ul>
					</li>
		
				</ul>
			</nav>
		</header>