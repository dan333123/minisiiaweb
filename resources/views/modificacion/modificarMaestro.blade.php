@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Alta-Matera")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/modificarMaestro') }}">
		{!! csrf_field() !!} <!-- csrf = cross-site request forgery (para evitar ataques de peticiones post al servidor y permitir a laravel la consulta post, aunque es posible desactivar el csrf en el archivo Kernerl.php)-->
		<fieldset>
			<legend>Modificar Maestro</legend>
            <p>
			
			@foreach ($datos as $maestro)
			<label for ="matricula">Matricula Maestro</label>: <input type="text" name = "matricula" id = "matricula" size = "30" maxlength = "20" placeholder="Matricula del maestro" value = "{{$maestro->matriculaMaestro}}"  autofocus required><br/> 
					<label for ="apellidoP">Apellido Paterno</label>: <input type="text" name = "apellidoP" id = "apellidoP" size = "30" maxlength = "20" placeholder="Apellido Paterno del maestro" value = "{{$maestro->apellidoP}}" required><br/> 
					<label for ="apellidoM">Apellido Materno</label>: <input type="text" name = "apellidoM" id = "apellidoM" size = "30" maxlength = "20" placeholder="Apellido Materno del maestro" value = "{{$maestro->apellidoM}}"><br/>
					<label for ="nombre">Nombre</label>: <input type="text" name = "nombre" id = "nombre" size = "30" maxlength = "15" placeholder="Nombre del maestro" required value = "{{$maestro->nombre}}"><br/>
					<label for ="departamento">Departamento</label>: <input type="text" name = "departamento" id = "departamento"size = "30" maxlength = "40" placeholder="Departamento del maestro" value = "{{$maestro->departamento}}" required><br/>
					<label for ="telefono">Telefono</label>: <input type="tel" name = "telefono" id = "telefono"size = "30" maxlength = "10" placeholder="Telefono particular" value = "{{$maestro->telefono}}" required><br/>
					<label for ="email">Correo electronico</label>: <input type="email" name = "email" id ="email" size = "30" maxlength = "25" placeholder="Correo electronico" value = "{{$maestro->correo}}" required><br/>
					<hr>
					<p>Genero:</p>
					<label for="mascu">Masculino</label><input type="radio" name="genero" value="Masculino" id="mascu" checked/><br/>
					<label for="feme">Femenino</label><input type="radio" name="genero" value="Femenino" id="feme" /><br/>
					<input type= "hidden" name = "matriculaAnterior" id = "matriculaAnterior" size = "30" maxlength = "20" placeholder="Matricula del maestro" value = "{{$maestro->matriculaMaestro}}">
					<hr>
			@endforeach		
				</p>
            <div class = "boton">
                <input type="submit" value="Modificar" ></code>	
            </div>
		</fieldset>	
	</form>
	<div class="resultado">
			<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')