@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Alta-Materia")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/modificarMateria') }}">
		{!! csrf_field() !!} <!-- csrf = cross-site request forgery (para evitar ataques de peticiones post al servidor y permitir a laravel la consulta post, aunque es posible desactivar el csrf en el archivo Kernerl.php)-->
		<fieldset>
			<legend>Modificar Materia</legend>
            <p>
			
			@foreach ($datos as $materia)
			        <label for ="clave">Clave</label>: <input type="number" name = "clave" id = "clave" size = "30" maxlength = "11" placeholder="Clave de la materia" value="{{$materia->clave}}" readonly required><br/> 
					<label for ="nombre">Materia</label>: <input type="text" name = "nombre" id = "nombre" size = "40" maxlength = "15" placeholder="Nombre de la materia" value="{{ $materia->nombre }}" autofocus required><br/> 
                    <label for ="carrera">Carrera</label>: <input type="text" name = "carrera" id = "carrera" size = "40" maxlength = "35" placeholder="Carrera" value="{{ $materia->carrera }}"required><br/> 
                    <label for ="unidades">Unidades</label>: <input type="number" name = "unidades" id = "unidades" size = "20" maxlength = "11" placeholder="Número de unidades" value="{{ $materia->unidades }}"required><br/>
					<label for ="id">Usuario</label>: <input type="text" name = "id" id = "id"size = "30" maxlength = "15" placeholder="usuario" value = "{{$materia->id}}" readonly ><br/>					
					<hr>
			@endforeach		
				</p>
            <div class = "boton">
                <input type="submit" value="Modificar" ></code>	
            </div>
		</fieldset>	
	</form>
	<div class="resultado">
			<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')