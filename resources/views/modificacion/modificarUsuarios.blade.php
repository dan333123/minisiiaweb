@include('partes.imagenCor');

@extends('layout_home')
@include('Menus.administradorMain');
@section('title', "Modificar-Usuario")

@section('estilos_adicionales')
	<link rel = "stylesheet" href = "{{ asset('css/Formulario.css') }}"> </link>
@endsection('estilos_adicionales')

@section('content')
	<form role="form" method="post" action="{{ url('/modificarUsuario') }}">
		{!! csrf_field() !!} <!-- csrf = cross-site request forgery (para evitar ataques de peticiones post al servidor y permitir a laravel la consulta post, aunque es posible desactivar el csrf en el archivo Kernerl.php)-->
		<fieldset>
			<legend>Modificar Usuario</legend>
            <p>
			
			@foreach ($datos as $usuarios)
					<label for ="usuario">ID</label>: <input type="text" name = "usuario" id = "usuario" size = "30" maxlength = "20" placeholder="Usuario" value = "{{$usuarios->id}}" disabled required><br/> 
					<label for ="Email">ID</label>: <input type="email" name = "Email" id = "Email" size = "30" maxlength = "20" placeholder="Usuario" value = "{{$usuarios->email}}" disabled required><br/> 
					<label for ="contra">Contraseña</label>: <input type="password" name = "contra" id = "contra" size = "30" maxlength = "20" placeholder="Contraseña" value = "{{$usuarios->password}}" required><br/> 
                    <label for ="tipo">Tipo</label>: <input type="text" name = "tipo" id = "tipo" size = "30" maxlength = "20" placeholder="Tipo" value = "{{$usuarios->Rol}}" disabled required><br/>
                    <input type= "hidden" name = "usuario2" id = "usuario2" size = "30" maxlength = "20" placeholder="Matricula del maestro" value = "{{$usuarios->id}}">
			@endforeach		
				</p>
            <div class = "boton">
                <input type="submit" value="Modificar" ></code>	
            </div>
		</fieldset>	
	</form>
	<div class="resultado">
			<p>{{$resultado}}</p>
	</div>
	@include('footer')
@endsection('content')