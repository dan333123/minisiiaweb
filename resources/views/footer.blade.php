<link rel = "stylesheet" href = "{{ asset('css/footer.css') }}"/>
<footer class="foot">
	<h3>Instituto Tecnológico de León</h3>
	<div class="campus">
		<div class="addressCampus">
			<h4 style="color:green;">CAMPUS I</h4>
			<p>
				Av. Tecnológico S/N - Fracc. Industrial Julián de Obregón León Guanajuato - C.P. 37290 Teléfono (477) 710 5200 - Fax (477) 711 2072
			</p>
		</div>
		<div class="addressCampus">
			<h4 style="color:green;">CAMPUS II</h4>
			<p>
				Juan Alonso de Torres No. 3542 - Col. San José de Piletas León Guanajuato - C.P. 37330 Teléfono (477) 710 5202 - Fax (477) 711 2072
			</p>
		</div>
	</div>
	<div id="generalInformation">
		<p>
			Sitio oficial del Instituto Tecnológico de León 
			<a style="color:#8E8CFF; text-decoration:none;" href="http://www.itleon.edu.mx" target="_blank">
				http://www.itleon.edu.mx
			</a>
			<br>E-mail: tecleon@itleon.edu.mx
			<a style="color:#8E8CFF; text-decoration:none;" href="https://www.facebook.com/OficialITLeon/" target="_blank">
				<img id="facebookIco" src="{{asset('images/icons/facebook.ico')}}"/>
			</a>
		</p>
		<p>Derechos reservados 
		<img src="{{asset('images/icons/light-copyright.ico')}}" 
			style="width:1.5%; margin:0%;padding:0%;transform:translateY(25%);"/>
		</p>
	</div>
</footer>