<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Auth.login');
});

/*Route::get('/home', "HomeController@index");

Route::get('/registro/maestro', "RegistrosController@registrarMaestro");

Route::get('/registro/alumno', "RegistrosController@registrarAlumno");

Route::get('/registro/materia', "RegistrosController@registrarMateria");*/

Route::post('/guardarMaestro', "maestroController@store");

Route::post('/guardarAlumno', "alumnoController@store");

Route::post('/guardarMateria', "materiaController@store");

Route::get('/materia/eliminar', "materiaController@datos");

Route::get('/materia/buscar', "materiaController@buscar");


Route::get('/materia/editar/{clave}', "materiaController@editar");

Route::get('/materia/eliminar/{clave}', "materiaController@eliminar");

Route::post('/modificarMateria', 'materiaController@modificar');

Route::get('/maestro/eliminar',"maestroController@datos");

Route::get('/maestro/buscar',"maestroController@buscar");

Route::post('/maestro/buscarParcial',"maestroController@buscarParcial");

Route::get('/maestro/editar/{matricula}',"maestroController@editar"); 

Route::get('/maestro/eliminar/{matricula}',"maestroController@eliminar"); 

Route::post('/modificarMaestro',"maestroController@modificar");

Route::get('/alumno/eliminar',"alumnoController@datos");

Route::get('/alumno/editar/{matricula}',"alumnoController@editar");

Route::post('/modificarAlumno',"alumnoController@modificar");

Route::get('/alumno/eliminar/{matricula}',"alumnoController@eliminar");

Route::get('/alumno/buscar',"alumnoController@buscar");

Route::post('/alumno/buscarParcial',"alumnoController@buscarParcial");

Route::get('/Administrar/semestre', "semestreController@semestre");

Route::post('/guardarSemestre', "semestreController@store");

Route::post('/guardarParciales', "semestreController@storeParcial");

Route::get('/semestre/eliminar/{codigoSemestre}',"semestreController@eliminar");

Route::get('/reporte/SemestreActual',"reSeActualAdminController@cursos");

Route::get('/Administrador/Reporte/Actual/Cursos/{Curso}',"reSeActualAdminController@curso");

Route::get('/Administrador/Reporte/Actual/CursoA/{Curso}',"reSeActualAdminController@cursoe");

Route::get('/Administrador/Reporte/Actual/Cursos/{Curso}/{parcial}',"reSeActualAdminController@cursoe2");

Route::get('/reporte/Semestre',"reSeActualAdminController@semestresPasados");

Route::get('/Administrador/Reporte/{Curso}',"reSeActualAdminController@semestrePasado");

Route::get('/extras/usuarios',"usuariosController@usuarios");

Route::get('/usuario/editar/{usuario}',"usuariosController@modificarE");

Route::get('/usuario/eliminar/{usuario}',"usuariosController@eliminar");

Route::post('/modificarUsuario', "usuariosController@modificar");

//Route::post('/guardarMaestro', ['as' => 'profile',  'uses' => 'maestroController@store']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/registro/maestro', "RegistrosController@registrarMaestro");

Route::get('/registro/alumno', "RegistrosController@registrarAlumno");

Route::get('/registro/materia', "RegistrosController@registrarMateria");

Route::get('/registro/curso', "cursoController@materia");

Route::get('/alta/curso/{clave}', "cursoController@materias");

Route::post('/guardarCurso',"cursoController@store");

Route::get('/curso/asignar', "cursoAlumnoController@cursosActuales");

Route::get('/asignar/curso/{claveMateria}', "cursoAlumnoController@agregarAlumnos");

Route::post('/guardarCursoAlumno', "cursoAlumnoController@store");

/* Rutas Alumnos */
Route::get('/materias',"alumnoController@materias");
Route::get('/calificacion/{materia}',"alumnoController@verCalificaciones");

Route::get('/maestro/cursos/actuales', "maestroCursosController@cursosActuales");

Route::get('/maestro/curso/calificar/{ClaveMateria}', "maestroCursosController@cursoCalificarParcial");

Route::get('/maestro/curso/calificar/{ClaveMateria}/{parcial}', "maestroCursosController@cursoCalificar");

Route::get('/maestro/curso/calificar/{ClaveMateria}/{parcial}/{matriculaAlumno}', "maestroCursosController@cursoCalificarAlumno");

Route::get('/maestro/cursos/calificar', "maestroCursosController@cursosActualesC");

Route::post('/calificarAlumno',"maestroCursosController@store");

Route::post('/actuCalificarAlumno',"maestroCursosController@actualizar");

Route::get('/maestro/cursos',"maestroCursosController@cursosActualesT");

Route::get('/maestro/calificaciones/{claveMateria}',"maestroController@verCalificaciones");

Route::get('/maestro/cursos/calificar/final',"caliFinalesController@cursosActuales");

Route::get('/maestro/cursos/calificar/final/{claveMateria}',"caliFinalesController@cursoCalificar");

Route::get('/maestro/cursos/calificar/final/{claveMateria}/{MatriculaAlumno}',"caliFinalesController@cursoCalificarAlumno");

Route::post('/calificarAlumnoFinal',"caliFinalesController@store");

Route::post('/actuCalificarAlumnoFinal',"caliFinalesController@actualizar");

Route::get('/maestro/ver/alumnos',"mastroCalificacionAlumnosController@alumnosMaestro");

Route::get('/maestro/ver/alumnos/{claveMateria}/{matriculaAlumno}',"mastroCalificacionAlumnosController@califAlumno");

Route::get('/maestro/ver/cursos/alumnos',"mastroCalificacionAlumnosController@cursosActuales");

Route::get('/maestro/ver/cursos/alumnos/{claveMateria}',"mastroCalificacionAlumnosController@alumnos");

Route::get('/curso/ver',"cursoController@cursosActivos");

Route::get('/curso/ver/{claveMateria}',"cursoController@alumnos");

//Route::get('/curso/eliminar/{claveMateria}',"cursoController@eliminar");

Route::get('/extras/califParcial',"modificarCalifController@modParcial");

Route::post('/claveParcial',"modificarCalifController@storeParcial"); 

Route::post('/reactivarCalifParcial',"modificarCalifController@reactivarParcial"); 

Route::get('/extras/califFinal',"modificarCalifController@modFinal");

Route::post('/claveFinal',"modificarCalifController@storeFinal"); 

Route::post('/reactivarCalifFinal',"modificarCalifController@reactivarFinal"); 



Route::get('/maestro/reporte',"maestroPDFController@cursosActuales");

Route::get('/maestro/reporte/{materia}/{claveMaestro}',"maestroPDFController@reporte");

Route::get('alumno/cursos/actuales/final',"alumnoController@mostrarFinales");
