<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PruebaModuloHome extends TestCase
{
    /**
    @test
     */
    public function load_home()
    {
        $response= $this->get('/home');
        $response->assertStatus(200);
    }
}
