<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
    @test
     */
    public function load_home()
    {
        $response= $this->get('/home');
        $response->assertStatus(200);
    }

    /**
     @test
     */
    function load_registro_maestro()
    {
        $this->get('/registro/maestro')
            ->assertStatus(200)
            ->assertSee('Datos del Maestro');
    }
}
