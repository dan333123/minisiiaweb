<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PruebaModuloRegistros extends TestCase
{
    /**
     @test
     */
    function load_registro_maestro()
    {
        $this->get('/registro/maestro')
            ->assertStatus(200)
            ->assertSee('Datos del Maestro');
    }
}